#!/usr/bin/env bash

OS=`uname | tr '[:upper:]' '[:lower:]'`
ARCH=`uname -m | tr '[:upper:]' '[:lower:]'`
VERSION="v0.4.0"
PACKAGE="https://gitee.com/Luna-CY/hui-hui/releases/download/${VERSION}/hui-hui_${OS}_${ARCH}_${VERSION}.tar.gz"

echo "curl -sSL -o /tmp/hui-hui.tar.gz $PACKAGE"
curl -sSL -o /tmp/hui-hui.tar.gz $PACKAGE

echo "创建目录: mkdir -p /opt/huihui"
mkdir -p /opt/huihui

echo "解压: tar zxf /tmp/hui-hui.tar.gz -C /opt/huihui"
tar zxf /tmp/hui-hui.tar.gz -C /opt/huihui

echo "清理安装包: rm -rf /tmp/hui-hui.tar.gz"
rm -rf /tmp/hui-hui.tar.gz

echo "安装完成"
