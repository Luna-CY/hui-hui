package main

import (
	"context"
	"fmt"
	"gitee.com/Luna-CY/hui-hui/command"
	_ "gitee.com/Luna-CY/hui-hui/internal/cache"
	"gitee.com/Luna-CY/hui-hui/internal/configure"
	"gitee.com/Luna-CY/hui-hui/internal/logger"
	"gitee.com/Luna-CY/hui-hui/internal/runtime"
)

func main() {
	var ctx, cancel = context.WithCancel(context.Background())
	runtime.RegisterSystemSignal(cancel, reload)

	if err := command.Execute(ctx); nil != err {
		fmt.Println(err)
	}
}

func reload() {
	if err := configure.Load(); nil != err {
		logger.GetLogger().Sugar().Error(err)
	}
}
