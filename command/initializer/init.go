package initializer

import (
	"github.com/spf13/cobra"
)

var description = `初始化系统环境，该命令将安装必须得最少组件以确保Hui-Hui能够成功运行。安装的组件列表在下方列出

组件列表
	- caddy WEB服务管理器，将作为此服务的前置服务，负责管理系统的80与443端口`

// overwrite 是否覆盖安装
// 指定此配置时清理系统内所有必要内容来确保能够成功初始化
var overwrite bool

// hostname 域名
// 初始化时必须设置一个域名用来申请SSL证书
// 该证书由Caddy服务管理
var hostname string

// skipCheckHostname 跳过检查：域名
var skipCheckHostname bool

// noTls 不起用TLS
var noTls bool

// tlsCert TLS证书路径
var tlsCert string

// tlsKey TLS私钥路径
var tlsKey string

func NewInitCommand() *cobra.Command {
	var command = &cobra.Command{
		Use:   "init",
		Short: "初始化系统环境，该命令将安装必须的最少组件以确保Hui-Hui能够成功运行",
		Long:  description,
		Args:  cobra.NoArgs,
		Run: func(cmd *cobra.Command, args []string) {
			if err := run(cmd, args); nil != err {
				cmd.Printf("失败: %s\n", err)
			}
		},
	}

	command.Flags().StringVar(&hostname, "hostname", "", "访问的域名，该域名必须解析到服务器的外部IP地址，灰灰将使用该域名申请SSL整数(此项必须)")
	command.Flags().BoolVar(&overwrite, "overwrite", false, "覆盖安装，请谨慎使用此参数\n设置该参数时将清理系统内被占用的资源及路径来确保能够成功初始化")
	command.Flags().BoolVar(&skipCheckHostname, "skip-check-hostname", false, "跳过检查域名，指定该选项时不检查域名是否正确解析")
	command.Flags().BoolVar(&noTls, "no-tls", false, "不使用TLS，不建议在服务器环境使用")
	command.Flags().StringVar(&tlsCert, "tls-cert", "", "自定义TLS证书，此参数指定TLS证书的路径，指定此参数时tls-key参数必须同时指定")
	command.Flags().StringVar(&tlsKey, "tls-key", "", "自定义TLS证书，此参数指定TLS私钥的路径，指定此参数时tls-cert参数必须同时指定")

	return command
}
