package reload

import "github.com/spf13/cobra"

func NewReloadCommand() *cobra.Command {
	var command = &cobra.Command{
		Use:   "reload",
		Short: "重载服务配置",
		Args:  cobra.NoArgs,
		RunE:  run,
	}

	return command
}
