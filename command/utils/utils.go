package utils

import (
	"gitee.com/Luna-CY/hui-hui/command/utils/reset_password"
	"github.com/spf13/cobra"
)

func NewUtilsCommand() *cobra.Command {
	var command = &cobra.Command{
		Use:   "utils",
		Short: "工具集",
		Args:  cobra.NoArgs,
	}

	command.AddCommand(reset_password.NewResetPasswordCommand())

	return command
}
