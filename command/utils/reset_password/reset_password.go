package reset_password

import (
	"crypto/md5"
	"fmt"
	"gitee.com/Luna-CY/hui-hui/internal/configure"
	"gitee.com/Luna-CY/hui-hui/internal/logger"
	"github.com/google/uuid"
	"github.com/spf13/cobra"
	"strings"
)

func NewResetPasswordCommand() *cobra.Command {
	return &cobra.Command{
		Use:   "reset-password",
		Short: "重置密码",
		Args:  cobra.NoArgs,
		RunE: func(cmd *cobra.Command, args []string) error {
			var password = strings.Split(uuid.New().String(), "-")[0]
			logger.GetLogger().Sugar().Infof("随机登录密码为: %s", password)

			configure.Configure.Server.Authentication.Type = configure.AuthenticationTypeFixed
			configure.Configure.Server.Authentication.Password = fmt.Sprintf("%x", md5.Sum([]byte(password)))

			return configure.SyncToDisk()
		},
	}
}
