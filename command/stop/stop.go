package stop

import "github.com/spf13/cobra"

func NewStopCommand() *cobra.Command {
	var command = &cobra.Command{
		Use:   "stop",
		Short: "停止Hui-Hui服务",
		Args:  cobra.NoArgs,
		RunE:  run,
	}

	return command
}
