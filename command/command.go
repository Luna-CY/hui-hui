package command

import (
	"context"
	"gitee.com/Luna-CY/hui-hui/command/initializer"
	"gitee.com/Luna-CY/hui-hui/command/reload"
	"gitee.com/Luna-CY/hui-hui/command/restart"
	"gitee.com/Luna-CY/hui-hui/command/run"
	"gitee.com/Luna-CY/hui-hui/command/start"
	"gitee.com/Luna-CY/hui-hui/command/stop"
	"gitee.com/Luna-CY/hui-hui/command/utils"
	"gitee.com/Luna-CY/hui-hui/internal/configure"
	"gitee.com/Luna-CY/hui-hui/internal/runtime"
	"github.com/spf13/cobra"
)

func Execute(ctx context.Context) error {
	if err := configure.Load(); nil != err {
		return err
	}

	var command = &cobra.Command{
		Use:     "hui-hui",
		Short:   "灰灰，我家的猫",
		Args:    cobra.NoArgs,
		Version: runtime.VersionName,
	}

	command.AddCommand(initializer.NewInitCommand(), start.NewStartCommand(), stop.NewStopCommand(), restart.NewRestartCommand(), reload.NewReloadCommand(), run.NewRunCommand(), utils.NewUtilsCommand())

	return command.ExecuteContext(ctx)
}
