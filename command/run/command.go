package run

import (
	"fmt"
	"gitee.com/Luna-CY/hui-hui/internal/configure"
	"github.com/spf13/cobra"
)

// httpListen 设置监听的地址，默认为127.0.0.1
var httpListen string

// httpPort 设置监听的地址，默认为8601
var httpPort int

func NewRunCommand() *cobra.Command {
	var command = &cobra.Command{
		Use:   "run",
		Short: "运行Hui-Hui服务，启动后可通过初始化时提供的域名进行访问",
		Long:  "运行Hui-Hui服务，该命令启动所有的统计任务与服务",
		Args:  cobra.NoArgs,
		RunE:  run,
	}

	command.Flags().StringVar(&httpListen, "http-listen", "127.0.0.1", "HTTP服务监听的本机地址，默认为127.0.0.1")
	command.Flags().IntVar(&httpPort, "http-port", configure.Configure.Server.Port, fmt.Sprintf("HTTP服务监听的本地端口号，默认为%d", configure.Configure.Server.Port))

	return command
}
