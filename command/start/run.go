package start

import (
	"gitee.com/Luna-CY/hui-hui/internal/util/syscall"
	"github.com/spf13/cobra"
)

func run(cmd *cobra.Command, _ []string) error {
	if _, err := syscall.ExecuteCommandGetOutputIgnoreQuitCode(cmd.Context(), "systemctl", "start", "hui-hui"); nil != err {
		return err
	}

	return nil
}
