package start

import "github.com/spf13/cobra"

var description = `启动Hui-Hui服务，默认启动为守护进程，如果需要在前台调试运行，请通过run命令启动`

func NewStartCommand() *cobra.Command {
	var command = &cobra.Command{
		Use:   "start",
		Short: "启动Hui-Hui服务",
		Long:  description,
		Args:  cobra.NoArgs,
		RunE:  run,
	}

	return command
}
