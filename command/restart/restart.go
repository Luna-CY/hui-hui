package restart

import "github.com/spf13/cobra"

func NewRestartCommand() *cobra.Command {
	var command = &cobra.Command{
		Use:   "restart",
		Short: "重启Hui-Hui服务",
		Args:  cobra.NoArgs,
		RunE:  run,
	}

	return command
}
