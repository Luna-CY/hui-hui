package response

// Response 公共响应结构
type Response[T any] struct {
	Code    int    `json:"code" validate:"required"`
	Message string `json:"message" validate:"required"`
	Data    T      `json:"data,omitempty" validate:"optional"`
}

// BaseListResponse 公共列表响应结构
type BaseListResponse[T any] struct {
	Total int64 `json:"total" validate:"required"`
	Data  []T   `json:"data" validate:"required"`
}
