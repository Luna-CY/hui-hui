package response

const Unknown = 999999

const (
	Ok = iota
	InvalidRequest
	NeedAuthentication
)

const (
	AuthorizeLoginUsernameOrPasswordInvalid = 100201
)
