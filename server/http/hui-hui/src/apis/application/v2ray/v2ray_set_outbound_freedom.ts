import { V2rayOutboundFreedom } from '@/apis/application/v2ray/structs.ts'
import { Api } from '@/apis/structs.ts'
import { api } from '@/apis/base.ts'
import { AxiosResponse } from 'axios'

export async function v2ray_set_outbound_freedom(bound: V2rayOutboundFreedom): Promise<Api> {
  const response = await api.post<Api, AxiosResponse<Api>>('/application/v2ray/set-outbound-freedom', bound)

  return response.data
}
