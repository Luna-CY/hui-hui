import { Api } from '@/apis/structs.ts'
import { api } from '@/apis/base.ts'
import { AxiosResponse } from 'axios'

export async function v2ray_stop(): Promise<Api> {
  const response = await api.post<Api, AxiosResponse<Api>>('/application/v2ray/stop')

  return response.data
}
