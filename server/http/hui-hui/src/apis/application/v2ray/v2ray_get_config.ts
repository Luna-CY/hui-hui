import { Api } from '@/apis/structs.ts'
import { api } from '@/apis/base.ts'
import { AxiosResponse } from 'axios'
import { V2rayStreamSettings } from '@/apis/application/v2ray/structs.ts'

export declare type V2rayConfig = {
  inbounds: Bound[],
  outbounds: Bound[]
}

export declare type Bound = {
  id: string,
  tag: string,
  protocol: string,
  inbound: {
    listen: string,
    port: number
  },
  outbound: {
    send_through: string
  },
  stream_settings: V2rayStreamSettings,
  freedom: {
    domain_strategy: string,
    redirect: string,
    user_level: number
  },
  vmess: {
    inbound: {
      disable_insecure_encryption: boolean,
      clients: VMessClient[]
    },
    outbound: {
      vnext: VMessVnext[]
    }
  },
  removing: boolean
}

export declare type VMessClient = {
  id: string,
  level: number,
  email: string,
  security: string
}

export declare type VMessVnext = {
  address: string,
  port: number,
  users: VMessClient[]
}

export async function v2ray_get_config(): Promise<Api<V2rayConfig>> {
  const response = await api.post<Api<V2rayConfig>, AxiosResponse<Api<V2rayConfig>>>('/application/v2ray/get-config')
  response.data.data = Object.assign(response.data.data, { removing: false })

  return response.data
}
