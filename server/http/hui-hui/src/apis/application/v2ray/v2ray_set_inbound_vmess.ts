import { V2rayInboundVMess } from '@/apis/application/v2ray/structs.ts'
import { Api } from '@/apis/structs.ts'
import { api } from '@/apis/base.ts'
import { AxiosResponse } from 'axios'

export async function v2ray_set_inbound_vmess(bound: V2rayInboundVMess): Promise<Api> {
  const response = await api.post<Api, AxiosResponse<Api>>('/application/v2ray/set-inbound-vmess', bound)

  return response.data
}
