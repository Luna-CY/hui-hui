// 创建一个新的未启用的传输配置对象
import { generate_random_string } from '@/funcs/random_string.ts'

export function new_v2ray_not_enable_stream_settings(): V2rayStreamSettings {
  return {
    enable: false,
    domain_socket_settings: { path: '/tmp/'+generate_random_string(16) },
    http2_settings: { host: [], path: '' },
    kcp_settings: { congestion: false, downlink_capacity: 20, header: { type: '' }, mtu: 1350, read_buffer_size: 2, tti: 50, uplink_capacity: 5, write_buffer_size: 2 },
    network: 'tcp', quic_settings: { header: { type: 'none' }, key: '', security: 'none' },
    tcp_settings: { header: { type: 'none' } },
    tls_settings: { client_only: false, security: 'none', server_name: '' },
    websocket_settings: { headers: {}, path: '/' }
  }
}

// 通用传输配置
// network为枚举值，可选项为: tcp/kcp/ws/domainsocket/http/quic
export declare type V2rayStreamSettings = {
  enable: boolean,
  network: string,
  tls_settings: {
    client_only: boolean,
    security: string,
    server_name: string
  },
  tcp_settings: {
    header: {
      type: string
    }
  },
  kcp_settings: {
    mtu: number,
    tti: number,
    uplink_capacity: number
    downlink_capacity: number,
    congestion: boolean,
    read_buffer_size: number,
    write_buffer_size: number,
    header: {
      type: string
    }
  },
  websocket_settings: {
    path: string,
    headers: { [key: string]: string }
  },
  domain_socket_settings: {
    path: string
  },
  http2_settings: {
    host: string[],
    path: string
  },
  quic_settings: {
    security: string,
    key: string,
    header: {
      type: string
    }
  }
}

export function new_v2ray_empty_vmess_client(): V2rayVMessClient {
  return { email: '', id: '', level: 0 }
}

// VMess入站协议的客户端配置
export declare type V2rayVMessClient = { id: string, level: number, email: string }

// 创建一个空的VMess入站配置对象
export function new_v2ray_inbound_empty_vmess(): V2rayInboundVMess {
  return { clients: [], disable_insecure_encryption: true, id: '', inbound_listen: '127.0.0.1', inbound_port: Math.floor(Math.random() * 9000) + 1000, stream_settings: new_v2ray_not_enable_stream_settings(), tag: '' }
}

// VMess入站协议配置
export declare type V2rayInboundVMess = { id: string, tag: string, inbound_listen: string, inbound_port: number, disable_insecure_encryption: boolean, clients: V2rayVMessClient[], stream_settings: V2rayStreamSettings }

// 创建一个空的Freedom出站配置对象
export function new_v2ray_outbound_freedom(): V2rayOutboundFreedom {
  return { domain_strategy: 'AsIs', id: '', outbound_send_through: '0.0.0.0', redirect: '', tag: '', user_level: 0, stream_settings: new_v2ray_not_enable_stream_settings() }
}

// Freedom出站协议配置
export declare type V2rayOutboundFreedom = {id: string, tag: string, outbound_send_through: string, domain_strategy: string, redirect: string, user_level: number, stream_settings: V2rayStreamSettings}

export function new_v2ray_outbound_vmess_vnext(): V2rayOutboundVMessVnext {
  return { address: '', port: 80, users: [] }
}

// VMess出站协议服务器配置
export declare type V2rayOutboundVMessVnext = {address: string, port: number, users: V2rayVMessClient[]}

// 创建一个空的Vmess出站协议
export function new_v2ray_outbound_vmess(): V2rayOutboundVMess {
  return { id: '', outbound_send_through: '0.0.0.0', stream_settings: new_v2ray_not_enable_stream_settings(), tag: '', vnext_list: [] }
}

// VMess出站协议配置
export declare type V2rayOutboundVMess = {id: string, tag: string, outbound_send_through: string, vnext_list: V2rayOutboundVMessVnext[], stream_settings: V2rayStreamSettings}
