export const APPLICATION_STATE_NOT_INSTALL = 1 // 未安装
export const APPLICATION_STATE_INSTALL_FAILURE = 2 // 安装失败
export const APPLICATION_STATE_VERSION_NOT_EFFECT = 3 // 版本无效
export const APPLICATION_STATE_INSTALLING = 4 // 安装中
export const APPLICATION_STATE_INSTALLED = 5 // 已安装
export const APPLICATION_STATE_STARTING = 6 // 启动中
export const APPLICATION_STATE_RESTARTING = 7 // 重新启动中
export const APPLICATION_STATE_RELOADING = 8 // 重新加载中
export const APPLICATION_STATE_RUNNING = 9 // 运行中
export const APPLICATION_STATE_STOPPING = 10 // 正在停止
export const APPLICATION_STATE_STOPPED = 11 // 已停止
export const APPLICATION_STATE_RUN_ERROR = 12 // 运行异常

export const APPLICATION_STATE_NAMES: {[key: number]: string} = {
  1: '未安装',
  2: '安装失败',
  3: '版本无效',
  4: '正在安装',
  5: '已安装',
  6: '启动中',
  7: '重新启动中',
  8: '配置重载中',
  9: '运行中',
  10: '停止中',
  11: '已停止',
  12: '运行异常'
}

// 通用应用状态
export declare type ApplicationState = { state: number, progress: number, stage: string, stage_progress: number, message: string, version: string }
