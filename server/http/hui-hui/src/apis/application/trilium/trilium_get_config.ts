import { Api } from '@/apis/structs.ts'
import { api } from '@/apis/base.ts'
import { AxiosResponse } from 'axios'

export declare type TriliumConfig = {listen: string, port: number}

export async function trilium_get_config(): Promise<Api<TriliumConfig>> {
  const response = await api.post<Api<TriliumConfig>, AxiosResponse<Api<TriliumConfig>>>('/application/trilium/get-config')

  return response.data
}
