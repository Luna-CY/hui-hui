import { Api } from '@/apis/structs.ts'
import { api } from '@/apis/base.ts'
import { AxiosResponse } from 'axios'

export async function trilium_install(overwrite: boolean): Promise<Api> {
  const response = await api.post<Api, AxiosResponse<Api>>('/application/trilium/install', { overwrite })

  return response.data
}
