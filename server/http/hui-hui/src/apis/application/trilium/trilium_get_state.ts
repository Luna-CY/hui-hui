import { Api } from '@/apis/structs.ts'
import { api } from '@/apis/base.ts'
import { AxiosResponse } from 'axios'
import { ApplicationState } from '@/apis/application/state.ts'

export async function trilium_get_state(): Promise<Api<ApplicationState>> {
  const response = await api.post<Api<ApplicationState>, AxiosResponse<Api<ApplicationState>>>('/application/trilium/get-state')

  return response.data
}
