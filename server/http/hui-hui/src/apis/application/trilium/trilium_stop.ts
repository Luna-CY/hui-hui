import { Api } from '@/apis/structs.ts'
import { api } from '@/apis/base.ts'
import { AxiosResponse } from 'axios'

export async function trilium_stop(): Promise<Api> {
  const response = await api.post<Api, AxiosResponse<Api>>('/application/trilium/stop')

  return response.data
}
