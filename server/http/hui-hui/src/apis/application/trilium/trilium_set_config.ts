import { Api } from '@/apis/structs.ts'
import { api } from '@/apis/base.ts'
import { AxiosResponse } from 'axios'

export async function trilium_set_config(listen: string, port: number): Promise<Api> {
  const response = await api.post<Api, AxiosResponse<Api>>('/application/trilium/set-config', { listen, port })

  return response.data
}
