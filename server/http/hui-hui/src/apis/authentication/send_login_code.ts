import { api } from '@/apis/base.ts'

export async function api_send_login_code(): Promise<void> {
  await api.post('/authentication/send-login-code')
}
