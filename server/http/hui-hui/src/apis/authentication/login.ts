import { Api } from '@/apis/structs.ts'
import { api } from '@/apis/base.ts'
import { AxiosResponse } from 'axios'

export declare type Token = { token: string }

export async function login(code: string): Promise<Api<Token>> {
  const response = await api.post<Api, AxiosResponse<Api<Token>>>('/authentication/login', { code })

  return response.data
}
