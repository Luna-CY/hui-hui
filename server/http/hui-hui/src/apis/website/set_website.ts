import {Website} from "@/apis/website/websites.ts";
import {Api} from "@/apis/structs.ts";
import {api} from "@/apis/base.ts";
import {AxiosResponse} from "axios";

export async function set_website(website: Website): Promise<Api> {
    const response = await api.post<Api, AxiosResponse<Api>>('/website/set', website)

    return response.data
}
