import { Api } from '@/apis/structs.ts'
import { api } from '@/apis/base.ts'
import { AxiosResponse } from 'axios'

export async function remove_website(hostname: string): Promise<Api> {
  const response = await api.post<Api, AxiosResponse<Api>>('/website/remove', { hostname })

  return response.data
}
