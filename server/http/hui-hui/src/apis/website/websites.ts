import { Api } from '@/apis/structs.ts'
import { api } from '@/apis/base.ts'
import { AxiosResponse } from 'axios'

export declare type Websites = {total: number, data: Website[]}
export declare type Website = {hostname: string, tls: {enable: boolean, custom: boolean, cert_path: string, key_path: string}, reverses: {path: string, target: string}[], removing: boolean}

export async function websites(): Promise<Api<Websites>> {
    const response = await api.post<Api<Websites>, AxiosResponse<Api<Websites>>>('/website/websites')

    return response.data
}
