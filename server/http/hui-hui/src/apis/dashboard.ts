import { Api } from '@/apis/structs.ts'
import { api } from '@/apis/base.ts'
import { AxiosResponse } from 'axios'

export declare type Dashboard = {
  cpus: number[],
  memory: {
    virtual: { total: number, used: number },
    swap: { total: number, used: number }
  },
  disk: {
    usage: { total: number, used: number }
    io_counter: { read: number, write: number}
  },
  network: {
    name: string,
    up: { total: number, current: number },
    down: { total: number, current: number },
  }[],
}

export async function dashboard(): Promise<Api<Dashboard>> {
  const response = await api.post<Api<Dashboard>, AxiosResponse<Api<Dashboard>>>('/basic/dashboard')

  return response.data
}
