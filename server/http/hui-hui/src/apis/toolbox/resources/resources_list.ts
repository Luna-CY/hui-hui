import { Api } from '@/apis/structs.ts'
import { api } from '@/apis/base.ts'
import { AxiosResponse } from 'axios'

export declare type ResourcesListResponse = { data: Entry[] }

export declare type Entry = { name: string, is_dir: boolean, size: number, ext: string, is_compressed_file: boolean, state: number, update_time: number, viewing: boolean, downloading: boolean, compressing: boolean, decompressing: boolean }

export async function resources_list(path: string): Promise<Api<ResourcesListResponse>> {
  const response = await api.post<Api<ResourcesListResponse>, AxiosResponse<Api<ResourcesListResponse>>>('/toolbox/resources/list', { path })

  return response.data
}
