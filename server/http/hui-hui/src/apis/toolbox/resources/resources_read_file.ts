import { Api } from '@/apis/structs.ts'
import { api } from '@/apis/base.ts'
import { AxiosResponse } from 'axios'

export declare type ReadFileResponse = { content: string }

export async function resources_read_file(path: string): Promise<Api<ReadFileResponse>> {
  const response = await api.post<Api<ReadFileResponse>, AxiosResponse<Api<ReadFileResponse>>>('/toolbox/resources/read-file', { path })

  return response.data
}
