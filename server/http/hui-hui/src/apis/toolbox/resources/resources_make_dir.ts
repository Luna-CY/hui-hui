import { Api } from '@/apis/structs.ts'
import { api } from '@/apis/base.ts'
import { AxiosResponse } from 'axios'

export async function resources_make_dir(path: string): Promise<Api> {
  const response = await api.post<Api, AxiosResponse<Api>>('/toolbox/resources/make-dir', { path })

  return response.data
}
