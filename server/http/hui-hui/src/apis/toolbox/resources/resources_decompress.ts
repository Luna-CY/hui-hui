import { Api } from '@/apis/structs.ts'
import { api } from '@/apis/base.ts'
import { AxiosResponse } from 'axios'

export async function resources_decompress(path: string): Promise<Api> {
  const response = await api.post<Api, AxiosResponse<Api>>('/toolbox/resources/decompress', { path })

  return response.data
}
