import { Api } from '@/apis/structs.ts'
import { api } from '@/apis/base.ts'
import { AxiosResponse } from 'axios'

export declare type UploadFileResponse = { path: string}

export async function resources_upload_file(data: FormData): Promise<Api<UploadFileResponse>> {
  const response = await api.post<Api<UploadFileResponse>, AxiosResponse<Api<UploadFileResponse>>>('/toolbox/resources/upload', data)

  return response.data
}
