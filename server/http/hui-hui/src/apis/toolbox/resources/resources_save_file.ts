import { Api } from '@/apis/structs.ts'
import { api } from '@/apis/base.ts'
import { AxiosResponse } from 'axios'

export async function resources_save_file(path: string, content: string): Promise<Api> {
  const response = await api.post<Api, AxiosResponse<Api>>('/toolbox/resources/save-file', { path, content })

  return response.data
}
