import { Api } from '@/apis/structs.ts'
import { api } from '@/apis/base.ts'
import { AxiosResponse } from 'axios'

export async function resources_compress(path: string, type: number): Promise<Api> {
  const response = await api.post<Api, AxiosResponse<Api>>('/toolbox/resources/compress', { path, type })

  return response.data
}
