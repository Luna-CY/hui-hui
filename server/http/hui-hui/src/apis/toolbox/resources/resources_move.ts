import { Api } from '@/apis/structs.ts'
import { api } from '@/apis/base.ts'
import { AxiosResponse } from 'axios'

export async function resources_move(source: string, target: string): Promise<Api> {
  const response = await api.post<Api, AxiosResponse<Api>>('/toolbox/resources/move', { source, target })

  return response.data
}
