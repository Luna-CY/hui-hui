import { Api } from '@/apis/structs.ts'
import { api } from '@/apis/base.ts'
import { AxiosResponse } from 'axios'

export declare type StartedTask = {id: string, start_time: number, filename: string, file_size: number}

export async function start_task(url: string, target: number, path: string, timeout: number): Promise<Api<StartedTask>> {
  const response = await api.post<Api<StartedTask>, AxiosResponse<Api<StartedTask>>>('/toolbox/downloader/start', {url, target, path, timeout})

  return response.data
}
