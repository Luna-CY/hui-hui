import { Api } from '@/apis/structs.ts'
import { api } from '@/apis/base.ts'
import { AxiosResponse } from 'axios'

export declare type Tasks = {total: number, data: Task[]}

export declare type Task = {id: string, state: number, message: string, start_time: number, end_time: number, filename: string, file_size: number, download_size: number, target: number, expired: number, local_file_removed: boolean, canceling: boolean, removing: boolean, retrying: boolean, downloading: boolean}

export async function tasks(): Promise<Api<Tasks>> {
    const response = await api.post<Api<Tasks>, AxiosResponse<Api<Tasks>>>('/toolbox/downloader/tasks')

    return response.data
}
