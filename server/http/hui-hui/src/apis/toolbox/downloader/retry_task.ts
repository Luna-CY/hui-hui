import { Api } from '@/apis/structs.ts'
import { api } from '@/apis/base.ts'
import { AxiosResponse } from 'axios'

export declare type RetryTask = { start_time: number }

export async function retry_task(id: string): Promise<Api<RetryTask>> {
  const response = await api.post<Api<RetryTask>, AxiosResponse<Api<RetryTask>>>('/toolbox/downloader/retry', { id })

  return response.data
}
