import { Api } from '@/apis/structs.ts'
import { api } from '@/apis/base.ts'
import { AxiosResponse } from 'axios'

export async function cancel_task(id: string): Promise<Api> {
  const response = await api.post<Api, AxiosResponse<Api>>('/toolbox/downloader/cancel', { id })

  return response.data
}
