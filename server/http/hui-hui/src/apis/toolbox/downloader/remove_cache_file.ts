import {Api} from "@/apis/structs.ts";
import {api} from "@/apis/base.ts";
import {AxiosResponse} from "axios";

export async function remove_cache_file(id: string): Promise<Api> {
    const response = await api.post<Api, AxiosResponse<Api>>('/toolbox/downloader/remove', {id})

    return response.data
}
