import { Api } from '@/apis/structs.ts'
import { api } from '@/apis/base.ts'
import { AxiosResponse } from 'axios'

export declare type ProgressResponse = { data: ProgressTask[] }

export declare type ProgressTask = { id: string, state: number, message: string, end_time: number, download_size: number, expired: number }

export async function query_progress(ids: string[]): Promise<Api<ProgressResponse>> {
  const response = await api.post<Api<ProgressResponse>, AxiosResponse<Api<ProgressResponse>>>('/toolbox/downloader/progress', { ids })

  return response.data
}
