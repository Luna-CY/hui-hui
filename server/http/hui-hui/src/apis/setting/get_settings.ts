import { Api } from '@/apis/structs.ts'
import { api } from '@/apis/base.ts'
import { AxiosResponse } from 'axios'

export declare type Settings = { auth: { type: string, email: string }, smtp: { host: string, port: number, username: string, password: string, from: string }, secret: { secret: string, content: string } }

export async function get_settings(): Promise<Api<Settings>> {
  const response = await api.post<Api, AxiosResponse<Api<Settings>>>('/settings/get')

  return response.data
}
