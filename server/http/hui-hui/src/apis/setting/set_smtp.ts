import {Api} from "@/apis/structs.ts";
import {api} from "@/apis/base.ts";
import {AxiosResponse} from "axios";

export async function set_smtp(host: string, port: number, username: string, password: string, from: string): Promise<Api> {
    const response = await api.post<Api, AxiosResponse<Api>>('/settings/set-smtp', {host, port, username, password, from})

    return response.data
}
