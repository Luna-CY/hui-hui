import { Api } from '@/apis/structs.ts'
import { api } from '@/apis/base.ts'
import { AxiosResponse } from 'axios'

export async function set_auth(type: string, password: string, email: string, secret: string, code: string): Promise<Api> {
  const response = await api.post<Api, AxiosResponse<Api>>('/settings/set-auth', { type, password, email, secret, code })

  return response.data
}
