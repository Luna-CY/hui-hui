import { api } from '@/apis/base.ts'
import { Api } from '@/apis/structs.ts'
import { AxiosResponse } from 'axios'

export declare type Token = {token: string}

export async function generate_download_token(): Promise<Api<Token>> {
  const response = await api.post<Api<Token>, AxiosResponse<Api<Token>>>('/basic/generate-download-token')

  return response.data
}
