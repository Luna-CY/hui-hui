export function date_format(timestamp: number, format: string) {
  const date = new Date(timestamp)
  const year = date.getFullYear()
  const month = date.getMonth() + 1
  const day = date.getDate()
  const hour = date.getHours()
  const minute = date.getMinutes()
  const second = date.getSeconds()

  const padLeftZero = (str: string) => {
    return ('0' + str).slice(-2)
  }

  format = format.replace('yyyy', year.toString())
  format = format.replace('MM', padLeftZero(month.toString()))
  format = format.replace('dd', padLeftZero(day.toString()))
  format = format.replace('HH', padLeftZero(hour.toString()))
  format = format.replace('mm', padLeftZero(minute.toString()))
  format = format.replace('ss', padLeftZero(second.toString()))

  return format
}
