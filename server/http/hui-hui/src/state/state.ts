import {defineStore} from 'pinia'

export const pinia_global_state = defineStore('pinia/global', {
    state: (): { _token: string } => {
        return {_token: ''}
    },
    getters: {
        token(): string {
            return this._token
        }
    },
    actions: {
        load(): void {
            let v = sessionStorage.getItem("JWT-TOKEN")
            if (null !== v && "" != v) {
                this._token = v
            }
        },
        set_token(token: string): void {
            this._token = token
            sessionStorage.setItem("JWT-TOKEN", token)
        }
    }
})
