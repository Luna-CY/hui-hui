import { createApp } from 'vue'
import App from './App.vue'
import { router } from '@/router/router.ts'
import { createPinia } from 'pinia'

import 'reset-css'
import '@/assets/css/global.css'
import { pinia_global_state } from '@/state/state.ts'

const app = createApp(App).use(router).use(createPinia())

const pinia = pinia_global_state()
pinia.load()

app.mount('#app')
