import { GlobalThemeOverrides } from 'naive-ui'

const def: GlobalThemeOverrides = {
  common: {
    fontFamily: "Roboto, -apple-system, BlinkMacSystemFont, 'Helvetica Neue', 'Segoe UI', 'Oxygen', 'Ubuntu', 'Cantarell', 'Open Sans', sans-serif",
    fontSize: '14px',
    fontWeight: '400',
    bodyColor: '#FBFCFC',
    textColorBase: '#2E4053',
    textColor1: '#2E4053',
    textColor2: '#2E4053',
    textColor3: '#2E4053',
    primaryColor: '#34495E',
    primaryColorHover: '#5D6D7E',
    primaryColorPressed: '#34495E',
    primaryColorSuppl: '#34495E',
    warningColor: '#E67E22',
    warningColorHover: '#EB984E',
    warningColorPressed: '#E67E22',
    warningColorSuppl: '#EB984E',
    errorColor: '#E74C3C',
    errorColorHover: '#EC7063',
    errorColorPressed: '#E74C3C',
    errorColorSuppl: '#EC7063',
    placeholderColor: '#AEB6BF'
  },
  Progress: {
    fillColor: '#E67E22',
    fillColorSuccess: '#34495E'
  },
  Tag: {
    border: 'none',
    color: '#FFFFFF',
    colorBordered: 'rgba(255, 255, 255, 1)'
  }
}

export { def }
