//go:build wireinject
// +build wireinject

package handler

import (
	"gitee.com/Luna-CY/hui-hui/internal/application/caddy"
	"gitee.com/Luna-CY/hui-hui/internal/application/trilium"
	"gitee.com/Luna-CY/hui-hui/internal/application/v2ray"
	"gitee.com/Luna-CY/hui-hui/internal/interface/application"
	"gitee.com/Luna-CY/hui-hui/internal/interface/service"
	"gitee.com/Luna-CY/hui-hui/internal/service/counter"
	"gitee.com/Luna-CY/hui-hui/internal/toolbox/crontab"
	"gitee.com/Luna-CY/hui-hui/internal/toolbox/downloader"
	"gitee.com/Luna-CY/hui-hui/internal/toolbox/processor"
	resources2 "gitee.com/Luna-CY/hui-hui/internal/toolbox/resources"
	trilium2 "gitee.com/Luna-CY/hui-hui/server/http/handler/application/trilium"
	v2ray2 "gitee.com/Luna-CY/hui-hui/server/http/handler/application/v2ray"
	"gitee.com/Luna-CY/hui-hui/server/http/handler/authentication"
	"gitee.com/Luna-CY/hui-hui/server/http/handler/basic"
	"gitee.com/Luna-CY/hui-hui/server/http/handler/settings"
	downloader2 "gitee.com/Luna-CY/hui-hui/server/http/handler/toolbox/downloader"
	"gitee.com/Luna-CY/hui-hui/server/http/handler/toolbox/resources"
	"gitee.com/Luna-CY/hui-hui/server/http/handler/website"
	"github.com/google/wire"
)

func NewBasicHandler() *basic.Basic {
	panic(wire.Build(
		crontab.Single,
		processor.Single,
		counter.Single,
		wire.Bind(new(service.Counter), new(*counter.Counter)),
		v2ray.Single,
		wire.Bind(new(application.V2ray), new(*v2ray.V2ray)),
		basic.New,
	))
}

func NewAuthenticationHandler() *authentication.Authentication {
	panic(wire.Build(
		authentication.New,
	))
}

func NewWebsiteHandler() *website.Website {
	panic(wire.Build(
		crontab.Single,
		processor.Single,
		caddy.Single,
		wire.Bind(new(application.Caddy), new(*caddy.Caddy)),
		website.New,
	))
}

func NewSettingsHandler() *settings.Settings {
	panic(wire.Build(
		settings.New,
	))
}

func NewToolboxDownloaderHandler() *downloader2.Downloader {
	panic(wire.Build(
		resources2.Single,
		downloader.Single,
		downloader2.New,
	))
}

func NewToolboxResourcesHandler() *resources.Resources {
	panic(wire.Build(
		resources2.Single,
		resources.New,
	))
}

func NewApplicationV2rayHandler() *v2ray2.V2ray {
	panic(wire.Build(
		crontab.Single,
		processor.Single,
		v2ray.Single,
		wire.Bind(new(application.V2ray), new(*v2ray.V2ray)),
		v2ray2.New,
	))
}

func NewApplicationTriliumHandler() *trilium2.Trilium {
	panic(wire.Build(
		crontab.Single,
		processor.Single,
		trilium.Single,
		wire.Bind(new(application.Trilium), new(*trilium.Trilium)),
		trilium2.New,
	))
}
