package settings

import (
	"gitee.com/Luna-CY/hui-hui/internal/configure"
	"gitee.com/Luna-CY/hui-hui/server/http/response"
	"github.com/gin-gonic/gin"
	"github.com/pquerna/otp/totp"
)

type GetResponse struct {
	Auth struct {
		Type  string `json:"type" validate:"required" enums:"fixed,email-code,totp"` // 验证类型
		Email string `json:"email" validate:"required"`                              // 接收验证码的邮件地址，仅在type为email-code时有效
	} `json:"auth" validate:"required"` // 身份验证配置
	Smtp struct {
		Host     string `json:"host" validate:"required"`     // 邮件服务器域名
		Port     int    `json:"port" validate:"required"`     // 邮件服务器端口
		Username string `json:"username" validate:"required"` // 验证用户名
		Password string `json:"password" validate:"required"` // 验证密码
		From     string `json:"from" validate:"required"`     // 发信人
	} `json:"smtp" validate:"required"` // 邮件服务器配置
	Secret struct {
		Secret  string `json:"secret" validate:"required"`  // Secret字符串
		Content string `json:"content" validate:"required"` // 用于生成二维码的内容字符串
	} `json:"secret" validate:"required"` // 一个新的用于生成TOTP二维码的配置数据
}

func (cls *Settings) Get(_ *gin.Context) (int, any, error) {
	var res = GetResponse{}

	res.Auth.Type = string(configure.Configure.Server.Authentication.Type)
	res.Auth.Email = configure.Configure.Server.Authentication.Email

	res.Smtp.Host = configure.Configure.Smtp.Host
	res.Smtp.Port = configure.Configure.Smtp.Port
	res.Smtp.Username = configure.Configure.Smtp.Username
	res.Smtp.Password = configure.Configure.Smtp.Password
	res.Smtp.From = configure.Configure.Smtp.From

	var secret, err = totp.Generate(totp.GenerateOpts{AccountName: "Admin", Issuer: configure.Configure.Server.Hostname})
	if nil != err {
		return response.Unknown, nil, err
	}

	res.Secret.Secret = secret.Secret()
	res.Secret.Content = secret.String()

	return response.Ok, res, nil
}
