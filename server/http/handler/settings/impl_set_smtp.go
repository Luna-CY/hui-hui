package settings

import (
	"gitee.com/Luna-CY/hui-hui/internal/configure"
	"gitee.com/Luna-CY/hui-hui/server/http/response"
	"github.com/gin-gonic/gin"
)

type SetSmtpRequest struct {
	Host     string `json:"host" validate:"required" binding:"required"`                                             // 服务器域名
	Port     int    `json:"port" validate:"required" minimum:"1" maximum:"65535" binding:"required,min=1,max=65535"` // 服务器端口号
	Username string `json:"username" validate:"optional" binding:"required_with=Password,omitempty"`                 // 验证用户名
	Password string `json:"password" validate:"optional" binding:"omitempty"`                                        // 验证密码
	From     string `json:"from" validate:"required" binding:"required,email"`                                       // 发信人
}

func (cls *Settings) SetSmtp(c *gin.Context) (int, any, error) {
	var body = SetSmtpRequest{}
	if err := c.ShouldBindJSON(&body); nil != err {
		return response.InvalidRequest, nil, err
	}

	configure.Configure.Smtp.Host = body.Host
	configure.Configure.Smtp.Port = body.Port
	configure.Configure.Smtp.Username = body.Username
	configure.Configure.Smtp.Password = body.Password
	configure.Configure.Smtp.From = body.From

	if err := configure.SyncToDisk(); nil != err {
		return response.Unknown, nil, err
	}

	return response.Ok, nil, nil
}
