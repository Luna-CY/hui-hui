package resources

import (
	"gitee.com/Luna-CY/hui-hui/server/http/response"
	"github.com/gin-gonic/gin"
	"strings"
)

type MakeDirRequest struct {
	Path string `json:"path" validate:"required" binding:"required"` // 路径
}

func (cls *Resources) MakeDir(c *gin.Context) (int, any, error) {
	var body = MakeDirRequest{}
	if err := c.ShouldBindJSON(&body); nil != err {
		return response.InvalidRequest, nil, err
	}

	if err := cls.resources.MakeDir(strings.Split(body.Path, "/")); nil != err {
		return response.Unknown, nil, err
	}

	return response.Ok, nil, nil
}
