package resources

import (
	"errors"
	"gitee.com/Luna-CY/hui-hui/internal/logger"
	"gitee.com/Luna-CY/hui-hui/server/http/response"
	"github.com/gin-gonic/gin"
	"strings"
)

type DecompressRequest struct {
	Path string `json:"path" validate:"required" binding:"required"`
}

func (cls *Resources) Decompress(c *gin.Context) (int, any, error) {
	var body = DecompressRequest{}
	if err := c.ShouldBindJSON(&body); nil != err {
		return response.InvalidRequest, nil, err
	}

	var paths = strings.Split(body.Path, "/")
	isCompressFile, err := cls.resources.CheckIsCompressedFile(paths)
	if nil != err {
		return response.Unknown, nil, err
	}

	if !isCompressFile {
		return response.InvalidRequest, nil, errors.New("此路径不是一个受支持的压缩文件")
	}

	go func() {
		if err := cls.resources.Decompress(paths); nil != err {
			logger.GetLogger().Sugar().Errorf("解压文件失败: %s %v", body.Path, err)
		}
	}()

	return response.Ok, nil, nil
}
