package resources

import "gitee.com/Luna-CY/hui-hui/internal/toolbox/resources"

func New(resources *resources.Resources) *Resources {
	return &Resources{resources: resources}
}

type Resources struct {
	resources *resources.Resources
}
