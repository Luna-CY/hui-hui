package resources

import (
	"gitee.com/Luna-CY/hui-hui/server/http/response"
	"github.com/gin-gonic/gin"
	"strings"
)

type ReadFileRequest struct {
	Path string `json:"path" validate:"required" binding:"required"`
}

type ReadFileResponse struct {
	Content string `json:"content" validate:"required"` // 内容
}

func (cls *Resources) ReadFile(c *gin.Context) (int, any, error) {
	var body = ReadFileRequest{}
	if err := c.ShouldBindJSON(&body); nil != err {
		return response.InvalidRequest, nil, err
	}

	content, err := cls.resources.ReadFile(strings.Split(body.Path, "/"))
	if nil != err {
		return response.Unknown, nil, err
	}

	return response.Ok, ReadFileResponse{Content: string(content)}, nil
}
