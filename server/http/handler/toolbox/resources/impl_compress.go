package resources

import (
	"errors"
	"gitee.com/Luna-CY/hui-hui/internal/logger"
	"gitee.com/Luna-CY/hui-hui/internal/toolbox/resources"
	"gitee.com/Luna-CY/hui-hui/server/http/response"
	"github.com/gin-gonic/gin"
	"strings"
)

type CompressRequest struct {
	Path string `json:"path" validate:"required" binding:"required"`
	Type int    `json:"type" validate:"required" enums:"1,2,3" binding:"required,oneof=1 2 3"`
}

func (cls *Resources) Compress(c *gin.Context) (int, any, error) {
	var body = CompressRequest{}
	if err := c.ShouldBindJSON(&body); nil != err {
		return response.InvalidRequest, nil, err
	}

	var paths = strings.Split(body.Path, "/")
	exists, err := cls.resources.CheckExists(paths)
	if nil != err {
		return response.Unknown, nil, err
	}

	if !exists {
		return response.InvalidRequest, nil, errors.New("路径不存在")
	}

	go func() {
		if err := cls.resources.Compress(paths, resources.ArchiveType(body.Type)); nil != err {
			logger.GetLogger().Sugar().Errorf("压缩文件失败: %s %v", body.Path, err)
		}
	}()

	return response.Ok, nil, nil
}
