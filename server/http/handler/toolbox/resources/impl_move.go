package resources

import (
	"gitee.com/Luna-CY/hui-hui/server/http/response"
	"github.com/gin-gonic/gin"
	"strings"
)

type MoveRequest struct {
	Source string `json:"source" validate:"required" binding:"required"` // 源路径
	Target string `json:"target" validate:"required" binding:"required"` // 目标路径
}

func (cls *Resources) Move(c *gin.Context) (int, any, error) {
	var body = MoveRequest{}
	if err := c.ShouldBindJSON(&body); nil != err {
		return response.InvalidRequest, nil, err
	}

	if err := cls.resources.Move(strings.Split(body.Source, "/"), strings.Split(body.Target, "/")); nil != err {
		return response.Unknown, nil, err
	}

	return response.Ok, nil, nil
}
