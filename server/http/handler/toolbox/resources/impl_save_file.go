package resources

import (
	"gitee.com/Luna-CY/hui-hui/server/http/response"
	"github.com/gin-gonic/gin"
	"strings"
)

type SaveFileRequest struct {
	Path    string `json:"path" validate:"required" binding:"required"`    // 路径
	Content string `json:"content" validate:"required" binding:"required"` // 内容
}

func (cls *Resources) SaveFile(c *gin.Context) (int, any, error) {
	var body = SaveFileRequest{}
	if err := c.ShouldBindJSON(&body); nil != err {
		return response.InvalidRequest, nil, err
	}

	if err := cls.resources.SaveFile(strings.Split(body.Path, "/"), strings.NewReader(body.Content)); nil != err {
		return response.Unknown, nil, err
	}

	return response.Ok, nil, nil
}
