package downloader

import (
	"gitee.com/Luna-CY/hui-hui/internal/toolbox/downloader"
	"gitee.com/Luna-CY/hui-hui/internal/toolbox/resources"
)

func New(downloader *downloader.Downloader, resources *resources.Resources) *Downloader {
	return &Downloader{downloader: downloader, resources: resources}
}

type Downloader struct {
	resources  *resources.Resources
	downloader *downloader.Downloader
}
