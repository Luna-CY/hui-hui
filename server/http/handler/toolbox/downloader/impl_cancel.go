package downloader

import (
	"gitee.com/Luna-CY/hui-hui/server/http/response"
	"github.com/gin-gonic/gin"
)

type CancelRequest struct {
	Id string `json:"id" validate:"required" binding:"required,uuid"` // 任务ID
}

func (cls *Downloader) Cancel(c *gin.Context) (int, any, error) {
	var body = CancelRequest{}
	if err := c.ShouldBindJSON(&body); nil != err {
		return response.InvalidRequest, nil, err
	}

	if err := cls.downloader.Cancel(body.Id); nil != err {
		return response.Unknown, nil, err
	}

	return response.Ok, nil, nil
}
