package downloader

import (
	"gitee.com/Luna-CY/hui-hui/server/http/response"
	"github.com/gin-gonic/gin"
)

type RemoveRequest struct {
	Id string `json:"id" validate:"required" binding:"required,uuid"` // 任务ID
}

func (cls *Downloader) Remove(c *gin.Context) (int, any, error) {
	var body = RemoveRequest{}
	if err := c.ShouldBindJSON(&body); nil != err {
		return response.InvalidRequest, nil, err
	}

	cls.downloader.Clean(&body.Id)

	return response.Ok, nil, nil
}
