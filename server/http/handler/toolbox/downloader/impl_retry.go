package downloader

import (
	"gitee.com/Luna-CY/hui-hui/server/http/response"
	"github.com/gin-gonic/gin"
)

type RetryRequest struct {
	Id string `json:"id" validate:"required" binding:"required,uuid"` // 任务ID
}

type RetryResponse struct {
	StartTime int64 `json:"start_time" validate:"required"` // 开始时间
}

func (cls *Downloader) Retry(c *gin.Context) (int, any, error) {
	var body = RetryRequest{}
	if err := c.ShouldBindJSON(&body); nil != err {
		return response.InvalidRequest, nil, err
	}

	var task, err = cls.downloader.Retry(body.Id)
	if nil != err {
		return response.Unknown, nil, err
	}

	return response.Ok, RetryResponse{StartTime: task.StartTime.Unix()}, nil
}
