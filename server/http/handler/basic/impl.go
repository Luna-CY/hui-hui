package basic

import (
	"gitee.com/Luna-CY/hui-hui/internal/interface/application"
	"gitee.com/Luna-CY/hui-hui/internal/interface/service"
)

func New(counter service.Counter, v2ray application.V2ray) *Basic {
	return &Basic{
		counter: counter,
		v2ray:   v2ray,
	}
}

type Basic struct {
	counter service.Counter
	v2ray   application.V2ray
}
