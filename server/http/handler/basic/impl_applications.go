package basic

import (
	"context"
	"fmt"
	"gitee.com/Luna-CY/hui-hui/server/http/response"
	"github.com/gin-gonic/gin"
)

type ApplicationsResponse struct {
	V2ray ApplicationsState `json:"v2Ray" validate:"required"` // V2ray服务状态
}

type ApplicationsState struct {
	State         int     `json:"state" validate:"required"`          // 服务状态：1-11
	Progress      float64 `json:"progress" validate:"required"`       // 整体进度
	Stage         string  `json:"stage" validate:"required"`          // 当前阶段
	StageProgress float64 `json:"stage_progress" validate:"required"` // 阶段进度
	Message       string  `json:"message" validate:"required"`        // 错误消息
}

func (cls *Basic) Applications(_ *gin.Context) (int, any, error) {
	var ctx = context.Background()
	if err := cls.v2ray.CheckLocationState(ctx); nil != err {
		return response.Unknown, nil, fmt.Errorf("检查V2ray服务状态失败: %s", err)
	}

	var res = ApplicationsResponse{}

	{
		var state, cp, message, stage, sp = cls.v2ray.State()
		res.V2ray.State = int(state)
		res.V2ray.Progress = cp
		res.V2ray.Stage = stage
		res.V2ray.StageProgress = sp
		res.V2ray.Message = message
	}

	return response.Ok, res, nil
}
