package basic

import (
	"crypto/md5"
	"fmt"
	"gitee.com/Luna-CY/hui-hui/internal/cache"
	"gitee.com/Luna-CY/hui-hui/server/http/response"
	"github.com/gin-gonic/gin"
	"github.com/google/uuid"
	"math/rand"
	"time"
)

type GenerateDownloadTokenResponse struct {
	Token string `json:"token" validate:"required"` // 临时Token，有效期一分钟
}

func (cls *Basic) GenerateDownloadToken(_ *gin.Context) (int, any, error) {
	var id = uuid.New().String()
	var token = fmt.Sprintf("%x%d", md5.Sum([]byte(id)), rand.Int())
	if err := cache.GetCache().Set(cache.AuthorizationTemporaryToken, token, 1*time.Minute); nil != err {
		return response.Unknown, nil, err
	}

	return response.Ok, GenerateDownloadTokenResponse{Token: token}, nil
}
