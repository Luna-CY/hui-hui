package website

import (
	"context"
	"errors"
	"gitee.com/Luna-CY/hui-hui/internal/configure"
	"gitee.com/Luna-CY/hui-hui/server/http/response"
	"github.com/gin-gonic/gin"
)

type RemoveRequest struct {
	Hostname string `json:"hostname" binding:"required,hostname"` // 域名
}

func (cls *Website) Remove(c *gin.Context) (int, any, error) {
	var body = RemoveRequest{}
	if err := c.ShouldBindJSON(&body); nil != err {
		return response.InvalidRequest, nil, err
	}

	if configure.Configure.Server.Hostname == body.Hostname {
		return response.InvalidRequest, nil, errors.New("不允许移除此站点")
	}

	var ctx = context.Background()
	if err := cls.caddy.RemoveWebsite(ctx, body.Hostname); nil != err {
		return response.Unknown, nil, err
	}

	// 应用配置
	if err := cls.caddy.ApplyConfig(ctx); nil != err {
		return response.Unknown, nil, err
	}

	// 重载服务
	if err := cls.caddy.Reload(ctx); nil != err {
		return response.Unknown, nil, err
	}

	return response.Ok, nil, nil
}
