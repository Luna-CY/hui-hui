package website

import "gitee.com/Luna-CY/hui-hui/internal/interface/application"

func New(caddy application.Caddy) *Website {
	return &Website{caddy: caddy}
}

type Website struct {
	caddy application.Caddy
}
