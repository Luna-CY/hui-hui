package website

import (
	"errors"
	"gitee.com/Luna-CY/hui-hui/server/http/response"
	"github.com/gin-gonic/gin"
)

type DetailRequest struct {
	Hostname string `json:"hostname" binding:"required"` // 域名
}

type DetailResponse struct {
	Hostname string `json:"hostname,hostname" validate:"required"` // 域名
	Tls      struct {
		Enable   bool   `json:"enable" validate:"required"`    // 是否启用TLS
		Custom   bool   `json:"custom" validate:"required"`    // 自定义TLS证书
		CertPath string `json:"cert_path" validate:"required"` // 证书路径
		KeyPath  string `json:"key_path" validate:"required"`  // 私钥路径
	} `json:"tls" validate:"required"` // TLS配置
	Reverses []DetailReverse `json:"reverses" validate:"required"` // 反向代理配置
}

type DetailReverse struct {
	Path   string `json:"path" validate:"required"`   // 匹配路径
	Target string `json:"target" validate:"required"` // 目标地址
}

func (cls *Website) Detail(c *gin.Context) (int, any, error) {
	var body = DetailRequest{}
	if err := c.BindJSON(&body); nil != err {
		return response.InvalidRequest, nil, err
	}

	var website, ok = cls.caddy.GetConfig().Websites[body.Hostname]
	if !ok {
		return response.InvalidRequest, nil, errors.New("域名不存在")
	}

	var res = DetailResponse{}

	res.Hostname = body.Hostname
	res.Tls.Enable = website.Tls.Enable
	res.Tls.Custom = website.Tls.Custom
	res.Tls.CertPath = website.Tls.CertPath
	res.Tls.KeyPath = website.Tls.KeyPath

	res.Reverses = make([]DetailReverse, 0)
	for _, reverse := range website.Reverses {
		res.Reverses = append(res.Reverses, DetailReverse{Path: reverse.Path, Target: reverse.Target})
	}

	return response.Ok, res, nil
}
