package trilium

import (
	"gitee.com/Luna-CY/hui-hui/internal/runtime"
	"gitee.com/Luna-CY/hui-hui/server/http/response"
	"github.com/gin-gonic/gin"
)

type GetStateResponse struct {
	State         int     `json:"state" validate:"required"`          // 服务状态：1-11
	Progress      float64 `json:"progress" validate:"required"`       // 整体进度
	Stage         string  `json:"stage" validate:"required"`          // 当前阶段
	StageProgress float64 `json:"stage_progress" validate:"required"` // 阶段进度
	Message       string  `json:"message" validate:"required"`        // 错误消息
	Version       string  `json:"version" validate:"required"`        // 版本号
}

func (cls *Trilium) GetState(_ *gin.Context) (int, any, error) {
	var res = GetStateResponse{}

	var state, gp, message, stage, sp = cls.trilium.State()
	res.State = int(state)
	res.Progress = gp
	res.Stage = stage
	res.StageProgress = sp
	res.Message = message
	res.Version = runtime.TriliumMinimumVersion

	return response.Ok, res, nil
}
