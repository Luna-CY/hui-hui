package trilium

import "gitee.com/Luna-CY/hui-hui/internal/interface/application"

func New(trilium application.Trilium) *Trilium {
	return &Trilium{trilium: trilium}
}

type Trilium struct {
	trilium application.Trilium
}
