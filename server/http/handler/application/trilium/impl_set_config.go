package trilium

import (
	"gitee.com/Luna-CY/hui-hui/server/http/response"
	"github.com/gin-gonic/gin"
)

type SetConfigRequest struct {
	Listen string `json:"listen" validate:"required" binding:"required,ip"`            // 监听地址
	Port   int    `json:"port" validate:"required" binding:"required,min=1,max=65535"` // 监听端口
}

func (cls *Trilium) SetConfig(c *gin.Context) (int, any, error) {
	var body = SetConfigRequest{}
	if err := c.ShouldBindJSON(&body); nil != err {
		return response.InvalidRequest, nil, err
	}

	var config = cls.trilium.GetConfig()
	config.Listen = body.Listen
	config.Port = body.Port

	if err := cls.trilium.SetConfig(config); nil != err {
		return response.Unknown, nil, err
	}

	return response.Ok, nil, nil
}
