package trilium

import (
	"context"
	"errors"
	"gitee.com/Luna-CY/hui-hui/internal/interface/application"
	"gitee.com/Luna-CY/hui-hui/internal/logger"
	"gitee.com/Luna-CY/hui-hui/internal/util/goroutine"
	"gitee.com/Luna-CY/hui-hui/server/http/response"
	"github.com/gin-gonic/gin"
)

func (cls *Trilium) Start(_ *gin.Context) (int, any, error) {
	logger.GetLogger().Sugar().Info("启动Trilium服务...")

	var state, _, _, _, _ = cls.trilium.State()
	if application.Installed != state && application.Stopped != state && application.RunError != state {
		return response.InvalidRequest, nil, errors.New("启动失败，Trilium服务当前状态不可启动，请稍后再试")
	}

	goroutine.Go(func() {
		if err := cls.trilium.Start(context.Background(), true); nil != err {
			logger.GetLogger().Sugar().Errorf("启动Trilium服务失败: %s", err)
		}
	})

	return response.Ok, nil, nil
}
