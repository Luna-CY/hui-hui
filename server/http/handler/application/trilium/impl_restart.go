package trilium

import (
	"context"
	"errors"
	"gitee.com/Luna-CY/hui-hui/internal/interface/application"
	"gitee.com/Luna-CY/hui-hui/internal/logger"
	"gitee.com/Luna-CY/hui-hui/internal/util/goroutine"
	"gitee.com/Luna-CY/hui-hui/server/http/response"
	"github.com/gin-gonic/gin"
)

func (cls *Trilium) Restart(_ *gin.Context) (int, any, error) {
	logger.GetLogger().Sugar().Info("重启Trilium服务...")

	var state, _, _, _, _ = cls.trilium.State()
	if application.Running != state {
		return response.InvalidRequest, nil, errors.New("Trilium服务当前状态不可重启，请稍后再试")
	}

	goroutine.Go(func() {
		if err := cls.trilium.Restart(context.Background()); nil != err {
			logger.GetLogger().Sugar().Errorf("重启Trilium服务失败: %s", err)
		}
	})

	return response.Ok, nil, nil
}
