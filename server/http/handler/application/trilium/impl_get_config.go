package trilium

import (
	"gitee.com/Luna-CY/hui-hui/server/http/response"
	"github.com/gin-gonic/gin"
)

type GetConfigResponse struct {
	Listen string `json:"listen" validate:"required"` // 监听地址
	Port   int    `json:"port" validate:"required"`   // 监听端口
}

func (cls *Trilium) GetConfig(_ *gin.Context) (int, any, error) {
	var config = cls.trilium.GetConfig()

	var res = GetConfigResponse{}
	res.Listen = config.Listen
	res.Port = config.Port

	return response.Ok, res, nil
}
