package trilium

import (
	"context"
	"gitee.com/Luna-CY/hui-hui/internal/logger"
	"gitee.com/Luna-CY/hui-hui/internal/util/goroutine"
	"gitee.com/Luna-CY/hui-hui/server/http/response"
	"github.com/gin-gonic/gin"
)

type InstallRequest struct {
	Overwrite bool `json:"overwrite" validate:"optional" binding:"omitempty"` // 是否强制覆盖
}

func (cls *Trilium) Install(c *gin.Context) (int, any, error) {
	var body = InstallRequest{}
	if err := c.ShouldBindJSON(&body); nil != err {
		return response.InvalidRequest, nil, err
	}

	goroutine.Go(func() {
		if err := cls.trilium.Install(context.Background(), body.Overwrite); nil != err {
			logger.GetLogger().Sugar().Errorf("安装Trilium服务失败: %s", err)
		}
	})

	return response.Ok, nil, nil
}
