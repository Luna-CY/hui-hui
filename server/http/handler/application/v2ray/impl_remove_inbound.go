package v2ray

import (
	"context"
	"gitee.com/Luna-CY/hui-hui/server/http/response"
	"github.com/gin-gonic/gin"
)

type RemoveInboundRequest struct {
	Id string `json:"id" validate:"required" binding:"required,uuid"` // 配置ID
}

func (cls *V2ray) RemoveInbound(c *gin.Context) (int, any, error) {
	var body = RemoveInboundRequest{}
	if err := c.ShouldBindJSON(&body); nil != err {
		return response.InvalidRequest, nil, err
	}

	var ctx = context.Background()
	if err := cls.v2ray.RemoveInbound(ctx, body.Id); nil != err {
		return response.Unknown, nil, err
	}

	if err := cls.v2ray.ApplyConfig(ctx); nil != err {
		return response.Unknown, nil, err
	}

	return response.Ok, nil, nil
}
