package http

import (
	"gitee.com/Luna-CY/hui-hui/server/http/handler"
	"gitee.com/Luna-CY/hui-hui/server/http/middleware"
	"github.com/gin-gonic/gin"
	"io/fs"
	"net/http"
	"strings"
)

// Register 注册路由
func Register(engine *gin.Engine) error {
	{
		var sub, _ = fs.Sub(Statistic, "hui-hui/dist")
		var httpFileSystem = http.FS(sub)

		engine.NoRoute(func(c *gin.Context) {
			// 文件
			if strings.Contains(c.Request.RequestURI, ".") {
				c.FileFromFS(c.Request.RequestURI, httpFileSystem)
				return
			}

			// 页面
			c.FileFromFS("/", httpFileSystem)
		})
	}

	var root = engine.Group("api")

	{
		var authentication = handler.NewAuthenticationHandler()
		var group = root.Group("authentication")

		group.POST("send-login-code", wrap(authentication.SendLoginCode))
		group.POST("login", wrap(authentication.Login))
	}

	// 需要鉴权
	root.Use(middleware.NewAuthentication())

	{
		var basic = handler.NewBasicHandler()
		var group = root.Group("basic")

		group.POST("dashboard", wrap(basic.Dashboard))
		group.POST("generate-download-token", wrap(basic.GenerateDownloadToken))
	}

	{
		var website = handler.NewWebsiteHandler()
		var group = root.Group("website")

		group.POST("websites", wrap(website.Websites))
		group.POST("detail", wrap(website.Detail))
		group.POST("set", wrap(website.Set))
		group.POST("remove", wrap(website.Remove))
	}

	{
		var settings = handler.NewSettingsHandler()
		var group = root.Group("settings")

		group.POST("get", wrap(settings.Get))
		group.POST("set-auth", wrap(settings.SetAuth))
		group.POST("set-smtp", wrap(settings.SetSmtp))
	}

	{
		var downloader = handler.NewToolboxDownloaderHandler()
		var group = root.Group("toolbox").Group("downloader")

		group.POST("tasks", wrap(downloader.Tasks))
		group.POST("start", wrap(downloader.Start))
		group.POST("cancel", wrap(downloader.Cancel))
		group.POST("progress", wrap(downloader.Progress))
		group.POST("remove", wrap(downloader.Remove))
		group.POST("retry", wrap(downloader.Retry))

		group.GET("download", downloader.Download)
	}

	{
		var resources = handler.NewToolboxResourcesHandler()
		var group = root.Group("toolbox").Group("resources")

		group.POST("list", wrap(resources.List))
		group.POST("make-dir", wrap(resources.MakeDir))
		group.POST("read-file", wrap(resources.ReadFile))
		group.POST("save-file", wrap(resources.SaveFile))
		group.POST("remove", wrap(resources.Remove))
		group.POST("move", wrap(resources.Move))
		group.POST("upload", wrap(resources.Upload))
		group.POST("compress", wrap(resources.Compress))
		group.POST("decompress", wrap(resources.Decompress))

		group.GET("download", resources.Download)
	}

	{
		var v2ray = handler.NewApplicationV2rayHandler()
		var group = root.Group("application").Group("v2ray")

		group.POST("get-state", wrap(v2ray.GetState))
		group.POST("get-config", wrap(v2ray.GetConfig))
		group.POST("install", wrap(v2ray.Install))
		group.POST("start", wrap(v2ray.Start))
		group.POST("stop", wrap(v2ray.Stop))
		group.POST("restart", wrap(v2ray.Restart))
		group.POST("set-inbound-vmess", wrap(v2ray.SetInboundVMess))
		group.POST("set-outbound-vmess", wrap(v2ray.SetOutboundVMess))
		group.POST("set-outbound-freedom", wrap(v2ray.SetOutboundFreedom))
		group.POST("remove-inbound", wrap(v2ray.RemoveInbound))
		group.POST("remove-outbound", wrap(v2ray.RemoveOutbound))
	}

	{
		var trilium = handler.NewApplicationTriliumHandler()
		var group = root.Group("application").Group("trilium")

		group.POST("get-state", wrap(trilium.GetState))
		group.POST("get-config", wrap(trilium.GetConfig))
		group.POST("install", wrap(trilium.Install))
		group.POST("start", wrap(trilium.Start))
		group.POST("stop", wrap(trilium.Stop))
		group.POST("restart", wrap(trilium.Restart))
		group.POST("set-config", wrap(trilium.SetConfig))
	}

	return nil
}
