package http

import (
	"gitee.com/Luna-CY/hui-hui/server/http/response"
	"github.com/gin-gonic/gin"
	"net/http"
)

// wrap 包装器
func wrap(handler func(c *gin.Context) (int, any, error)) gin.HandlerFunc {
	return func(c *gin.Context) {
		var v response.Response[any]

		var code, content, err = handler(c)
		if response.Ok != code || nil != err {
			v.Code = code
			v.Message = "NONE"

			if nil != err {
				v.Message = err.Error()
			}

			c.JSON(http.StatusOK, v)

			return
		}

		v.Code = code
		v.Message = "OK"
		v.Data = content

		c.JSON(http.StatusOK, v)
	}
}
