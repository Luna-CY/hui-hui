package logger

import (
	"go.uber.org/zap"
	"go.uber.org/zap/zapcore"
)

var logger *zap.Logger

func init() {
	var encoder = zap.NewDevelopmentEncoderConfig()
	encoder.CallerKey = ""
	encoder.FunctionKey = ""
	encoder.StacktraceKey = ""

	var config = zap.NewProductionConfig()
	config.Encoding = "console"
	config.EncoderConfig = encoder
	config.Level.SetLevel(zapcore.InfoLevel)

	var log, err = config.Build()
	if nil != err {
		panic(err)
	}

	logger = log
}

func GetLogger() *zap.Logger {
	return logger
}
