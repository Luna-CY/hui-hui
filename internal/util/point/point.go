package point

func New[T any](v T) *T {
	return &v
}

func ErrorMessage(err error) string {
	if nil == err {
		return ""
	}

	return err.Error()
}
