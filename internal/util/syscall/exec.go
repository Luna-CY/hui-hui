package syscall

import (
	"context"
	"gitee.com/Luna-CY/hui-hui/internal/logger"
	"os/exec"
	"strings"
)

// ExecuteCommandGetOutputIgnoreQuitCode 执行命令并获取输出（含stderr）
// 此操作忽略命令的退出状态
func ExecuteCommandGetOutputIgnoreQuitCode(ctx context.Context, command string, args ...string) ([]string, error) {
	args = append([]string{command}, args...)
	command = strings.Join(args, " ")

	logger.GetLogger().Sugar().Infof("执行系统命令: %s", command)
	var c = exec.CommandContext(ctx, "/bin/bash", "-c", command)

	var output, err = c.CombinedOutput()
	if nil != err && 1 != c.ProcessState.ExitCode() {
		return nil, err
	}

	var rows = strings.Split(strings.TrimSpace(string(output)), "\n")
	if 1 == len(rows) && "" == rows[0] {
		return nil, nil
	}

	return rows, nil
}
