package syscall

// CommandQueryHttpAndHttpsPort 查询本地80、443端口是否有进程在监听
var CommandQueryHttpAndHttpsPort = []string{"netstat", "-ntlp", "|", "grep", "ESTABLISHED", "|", "awk", "'{print $4}'", "|", "grep", "-E", "\":80$|:443$\""}
