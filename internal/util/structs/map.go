package structs

// MapValues 获取Map结构的值列表
func MapValues[K comparable, V any](m map[K]V) []V {
	var vs []V
	for _, v := range m {
		vs = append(vs, v)
	}

	return vs
}

// MapGetOrDefault 返回给定key的value或者default
func MapGetOrDefault[K comparable, V any](m map[K]V, k K, d V) V {
	if v, ok := m[k]; ok {
		return v
	}

	return d
}
