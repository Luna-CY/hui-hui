package network

import (
	"context"
	"net"
	"strings"
)

// LookupHostname 解析域名绑定的ipv4与ipv6地址
func LookupHostname(ctx context.Context, hostname string) (net.IP, net.IP, error) {
	var address, err = net.DefaultResolver.LookupIPAddr(ctx, hostname)
	if nil != err {
		return nil, nil, err
	}

	var ipv4 net.IP
	var ipv6 net.IP

	for _, ad := range address {
		// ipv4地址可以嵌入ipv6地址来进行兼容，检查时需判断是否不含:
		if strings.Contains(ad.IP.String(), ".") || !strings.Contains(ad.IP.String(), ":") {
			ipv4 = ad.IP
		} else if strings.Contains(ad.IP.String(), ":") {
			ipv6 = ad.IP
		}
	}

	return ipv4, ipv6, nil
}
