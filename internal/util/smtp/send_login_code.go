package smtp

import (
	"gitee.com/Luna-CY/hui-hui/internal/configure"
	"gopkg.in/gomail.v2"
	"strings"
)

const email = `<!DOCTYPE html>
<html lang="zh">
	<head>
		<title>[Hui-Hui] 登录验证码</title>
		<link rel="stylesheet" href="https://unpkg.com/reset-css/reset.css" />
	</head>
	<body style="font-family: -apple-system, BlinkMacSystemFont, Tahoma, Arial, 'Hiragino Sans GB', 'Microsoft YaHei', sans-serif; font-size: 14px; margin-top: 60px;">
		<div style="display: flex; justify-content: center; margin-top: 120px;">
			<h3>您的登录验证码为: {CODE}，此验证码在15分钟内有效，请不要将此验证码告知其他任何人</h3>
		</div>
	</body>
</html>`

// SendLoginCode 发送登录验证码
func SendLoginCode(to string, code string) error {
	var message = gomail.NewMessage()
	message.SetHeader("From", configure.Configure.Smtp.From)
	message.SetHeader("To", to)
	message.SetHeader("Subject", "[Hui-Hui] 登录验证码")
	message.SetBody("text/html", strings.Replace(email, "{CODE}", code, 1))

	if err := gomail.NewDialer(configure.Configure.Smtp.Host, configure.Configure.Smtp.Port, configure.Configure.Smtp.Username, configure.Configure.Smtp.Password).DialAndSend(message); nil != err {
		return err
	}

	return nil
}
