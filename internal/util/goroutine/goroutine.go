package goroutine

import "sync"

// Go 启动goroutine
func Go(fn func()) {
	var wg sync.WaitGroup
	wg.Add(1)

	go func() {
		wg.Done()

		fn()
	}()

	wg.Wait()
}
