package archive

import (
	"github.com/cheggaaa/pb/v3"
	"io"
)

func NewProxyReaderAt(reader io.ReaderAt, bar *pb.ProgressBar, ch chan int64) *ReaderAt {
	bar.Set(pb.Bytes, true)
	return &ReaderAt{reader: reader, bar: bar, ch: ch}
}

type ReaderAt struct {
	reader io.ReaderAt
	bar    *pb.ProgressBar
	ch     chan int64
}

func (cls *ReaderAt) ReadAt(p []byte, off int64) (n int, err error) {
	n, err = cls.reader.ReadAt(p, off)
	cls.bar.Add(n)
	cls.ch <- int64(n)

	return
}

func (cls *ReaderAt) Close() error {
	close(cls.ch)

	return nil
}
