package archive

import (
	"archive/tar"
	"compress/gzip"
	"context"
	"gitee.com/Luna-CY/hui-hui/internal/util/goroutine"
	"github.com/cheggaaa/pb/v3"
	"io"
	"os"
	"path/filepath"
	"strings"
)

func CompressGzipFileFromPath(_ context.Context, from string, to string) error {
	// TODO
	return nil
}

// DecompressGzipFile 解压.gz文件
func DecompressGzipFile(_ context.Context, file *os.File, size int64, target string, callback func(total int64, current int64)) error {
	// 控制台进度条
	var bar = pb.Full.Start64(size)
	var proxy = &reader{r: bar.NewProxyReader(file), ch: make(chan int64, 100)}

	defer func() {
		bar.Finish()
		_ = proxy.Close()
	}()

	gr, err := gzip.NewReader(proxy)
	if nil != err {
		return err
	}

	defer func() {
		_ = gr.Close()
	}()

	// 进度反馈
	goroutine.Go(func() {
		var current int64
		for n := range proxy.ch {
			current += n
			if nil != callback {
				callback(size, current)
			}
		}
	})

	var tokens = strings.Split(file.Name(), ".")
	f, err := os.OpenFile(filepath.Join(target, strings.Join(tokens[:len(tokens)-1], ".")), os.O_CREATE|os.O_TRUNC|os.O_WRONLY, 0644)
	if nil != err {
		return err
	}

	defer func() {
		_ = f.Close()
	}()

	if _, err := io.Copy(f, gr); nil != err {
		return err
	}

	return nil
}

// DecompressTarGzipFile 解压tar.gz文件
func DecompressTarGzipFile(ctx context.Context, file *os.File, size int64, target string, callback func(total int64, current int64)) error {
	// 控制台进度条
	var bar = pb.Full.Start64(size)
	var proxy = &reader{r: bar.NewProxyReader(file), ch: make(chan int64, 100)}

	defer func() {
		bar.Finish()
		_ = proxy.Close()
	}()

	gr, err := gzip.NewReader(proxy)
	if nil != err {
		return err
	}

	defer func() {
		_ = gr.Close()
	}()

	// 进度反馈
	goroutine.Go(func() {
		var current int64
		for n := range proxy.ch {
			current += n
			if nil != callback {
				callback(size, current)
			}
		}
	})

	var tr = tar.NewReader(gr)
	for hdr, err := tr.Next(); io.EOF != err; hdr, err = tr.Next() {
		if nil != err {
			return err
		}

		if nil != ctx.Err() {
			return ctx.Err()
		}

		if err := os.MkdirAll(filepath.Join(target, filepath.Dir(hdr.Name)), 0755); nil != err {
			return err
		}

		if hdr.FileInfo().IsDir() {
			continue
		}

		if tar.TypeLink == hdr.Typeflag || tar.TypeSymlink == hdr.Typeflag {
			if err := os.Symlink(hdr.Linkname, filepath.Join(target, hdr.Name)); nil != err {
				return err
			}

			continue
		}

		f, err := os.OpenFile(filepath.Join(target, hdr.Name), os.O_CREATE|os.O_TRUNC|os.O_WRONLY, hdr.FileInfo().Mode())
		if nil != err {
			return err
		}

		if _, err := io.Copy(f, tr); nil != err {
			return err
		}

		_ = f.Close()
	}

	return nil
}
