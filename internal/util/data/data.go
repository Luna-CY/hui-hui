package data

import (
	"reflect"
)

// Default 返回给定值或默认值
func Default[T any](v T, d T) T {
	if reflect.ValueOf(v).IsZero() {
		return d
	}

	return v
}

// CheckNumberRange 检查数值类型的范围
func CheckNumberRange[T ~int8 | ~int | ~int16 | ~int32 | ~int64 | ~uint8 | ~uint | ~uint16 | ~uint32 | ~uint64 | ~float32 | ~float64](v T, min T, max T) bool {
	if min > v || max < v {
		return false
	}

	return true
}
