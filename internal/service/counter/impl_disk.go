package counter

func (cls *Counter) diskIOCount(read uint64, write uint64) {
	cls.disk.read = read
	cls.disk.write = write
}

func (cls *Counter) DiskGetIOCount() (uint64, uint64) {
	return cls.disk.read, cls.disk.write
}
