package counter

func (cls *Counter) networkIOCount(name string, up uint64, down uint64) {
	cls.network.cm.Lock()
	defer cls.network.cm.Unlock()

	cls.network.current[name] = [2]uint64{up, down}

	cls.network.tm.Lock()
	defer cls.network.tm.Unlock()

	var data = [2]uint64{0, 0}
	if v, ok := cls.network.total[name]; ok {
		data = v
	}

	data[0] += up
	data[1] += down

	cls.network.total[name] = data
}

func (cls *Counter) NetworkGetIOCount(name string) (uint64, uint64, uint64, uint64) {
	cls.network.cm.RLock()
	cls.network.cm.RUnlock()

	cls.network.tm.RLock()
	defer cls.network.tm.RUnlock()

	var data = [2]uint64{0, 0}
	if v, ok := cls.network.total[name]; ok {
		data = v
	}

	return cls.network.current[name][0], cls.network.current[name][1], data[0], data[1]
}

func (cls *Counter) networkReset() {
	cls.network.cm.Lock()
	defer cls.network.cm.Unlock()

	for name := range cls.network.current {
		cls.network.current[name] = [2]uint64{0, 0}
	}

	cls.network.tm.Lock()
	defer cls.network.tm.Unlock()

	for name := range cls.network.total {
		cls.network.total[name] = [2]uint64{0, 0}
	}
}
