package service

type Counter interface {
	// NetworkGetIOCount 获取网络IO统计数
	// name 为网卡名称
	// 返回统计的当前上行、当前下行、上行总量、下行总量
	NetworkGetIOCount(name string) (cu uint64, cd uint64, tu uint64, td uint64)

	// DiskGetIOCount 获取磁盘IO统计
	DiskGetIOCount() (uint64, uint64)
}
