package application

import "context"

type State int

const (
	NotInstall       = State(iota) + 1 // 未安装
	InstallFailure                     // 安装失败
	VersionNotEffect                   // 版本不可使用
	Installing                         // 安装中
	Installed                          // 已安装
	Starting                           // 启动中
	Restarting                         // 重启中
	Reloading                          // 重载中
	Running                            // 已启动
	Stopping                           // 停止中
	Stopped                            // 已停止
	RunError                           // 运行异常
)

// Application 基础应用接口定义
type Application interface {
	// CheckLocationState 检查本地状态
	CheckLocationState(ctx context.Context) error

	// State 返回应用当前的状态
	// 依次返回状态、总进度百分比、当前阶段、阶段进度百分比
	State() (State, float64, string, string, float64)

	// Install 安装，overwrite参数指定是否覆盖安装
	Install(ctx context.Context, overwrite bool) error

	// ApplyConfig 应用配置
	// 该操作会使用Hui-Hui保存的配置信息覆写应用的配置信息
	ApplyConfig(ctx context.Context) error

	// Start 启动应用，restart参数指定是否应该在进程意外终止时重启应用
	// 该方法应该在启动成功后返回，而不是阻塞
	Start(ctx context.Context, restart bool) error

	// Restart 重启服务
	Restart(ctx context.Context) error

	// Reload 重载服务配置，注意部分应用可能不支持
	Reload(ctx context.Context) error

	// Stop 停止应用，该方法应该阻塞到应用停止后返回
	Stop(ctx context.Context) error
}
