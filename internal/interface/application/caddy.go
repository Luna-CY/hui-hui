package application

import "context"

// Caddy Caddy应用接口定义
type Caddy interface {
	Application

	// GetConfig 获取配置对象
	// 不允许直接修改配置对象
	GetConfig() *CaddyConfig

	// SetBasic 保存基础配置
	SetBasic(ctx context.Context, basic CaddyBasic) error

	// SetWebsite 添加站点配置
	// hostname不能包含协议信息
	SetWebsite(ctx context.Context, hostname string, reverses []*CaddyWebsiteReverse, tls *CaddyWebsiteTls) error

	// RemoveWebsite 移除站点配置
	RemoveWebsite(ctx context.Context, hostname string) error
}

// CaddyConfig Caddy的配置结构
type CaddyConfig struct {
	Basic    CaddyBasic               `json:"basic"`    // 基础配置
	Websites map[string]*CaddyWebsite `json:"websites"` // 站点配置
}

type CaddyBasic struct {
}

type CaddyWebsite struct {
	Hostname string                 `json:"hostname"` // 域名，不得包含协议
	Tls      *CaddyWebsiteTls       `json:"tls"`      // TLS配置
	Reverses []*CaddyWebsiteReverse `json:"reverses"` // 反向代理配置
}

type CaddyWebsiteTls struct {
	Enable   bool   `json:"enable"`   // 是否启用TLS
	Custom   bool   `json:"custom"`   // 自定义证书配置
	CertPath string `json:"pem_path"` // 证书路径
	KeyPath  string `json:"key_path"` // 私钥路径
}

type CaddyWebsiteReverse struct {
	Path   string `json:"path"`   // 代理的路径
	Target string `json:"target"` // 反向代理目标地址，需要包含协议、地址、端口三个部分
}
