package application

type Trilium interface {
	Application

	// GetConfig 获取配置
	GetConfig() *TriliumConfig

	// SetConfig 应用配置
	SetConfig(config *TriliumConfig) error
}

type TriliumConfig struct {
	Listen string `json:"listen"` // 监听地址，默认127.0.0.1
	Port   int    `json:"port"`   // 监听端口，默认8602
}
