package runner

import "context"

// Runner
// 该函数在成功时返回一个通道，该通道必须在Runner退出时返回一个信号
type Runner func(ctx context.Context) (<-chan struct{}, error)
