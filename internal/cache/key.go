package cache

// AuthorizationSecret 登录验证的随机串
const AuthorizationSecret = "authentication-secret"

// AuthorizationCode 登录验证码
const AuthorizationCode = "authentication-code"

const AuthorizationTemporaryToken = "authorization-temporary-token"
