package cache

import (
	"encoding/json"
	"errors"
	"gitee.com/Luna-CY/hui-hui/internal/logger"
	"github.com/allegro/bigcache/v3"
	"time"
)

// Get 获取缓存值
func (cls *Cache) Get(key string) (string, error) {
	var v, err = cls.cache.Get(key)
	if nil != err {
		if bigcache.ErrEntryNotFound == err {
			return "", nil
		}

		logger.GetLogger().Sugar().Errorw("获取缓存失败", "key", key, "err", err)

		return "", errors.New("查询缓存失败")
	}

	var value Value
	if err := json.Unmarshal(v, &value); nil != err {
		logger.GetLogger().Sugar().Errorw("解析缓存失败", "key", key, "value", string(v), "err", err)

		return "", errors.New("查询缓存失败")
	}

	// 检查过期
	var ttl = int(time.Unix(value.Expired, 0).Sub(time.Now()).Seconds())
	if 0 > ttl {
		return "", nil
	}

	return string(value.Bytes), nil
}
