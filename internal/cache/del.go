package cache

import (
	"errors"
	"gitee.com/Luna-CY/hui-hui/internal/logger"
)

// Del 删除给定的key
func (cls *Cache) Del(key string) error {
	if err := cls.cache.Delete(key); nil != err {
		logger.GetLogger().Sugar().Errorw("Cache Del Fail.", "key", key, "err", err)

		return errors.New("删除缓存失败")
	}

	return nil
}
