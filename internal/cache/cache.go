package cache

import (
	"context"
	"gitee.com/Luna-CY/hui-hui/internal/configure"
	"github.com/allegro/bigcache/v3"
	"github.com/gin-gonic/gin"
	"io"
	"log"
	"sync"
	"time"
)

var once sync.Once
var bc *Cache

func init() {
	once.Do(func() {
		var config = bigcache.DefaultConfig(10 * time.Minute)
		config.MaxEntrySize = 512
		config.MaxEntriesInWindow = 1024
		config.HardMaxCacheSize = 2 // MB

		// 生产环境不启用日志
		if gin.ReleaseMode == configure.Configure.Server.GinMode {
			config.Logger = log.New(io.Discard, "", log.LstdFlags)
		}

		var cache, err = bigcache.New(context.Background(), config)
		if nil != err {
			panic(err)
		}

		bc = &Cache{cache: cache}
	})
}

func GetCache() *Cache {
	return bc
}

type Cache struct {
	cache *bigcache.BigCache
}

type Value struct {
	Bytes   []byte `json:"bytes"`   // 数据内容
	Expired int64  `json:"expired"` // 过期时间，10位时间戳
}
