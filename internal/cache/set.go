package cache

import (
	"encoding/json"
	"errors"
	"gitee.com/Luna-CY/hui-hui/internal/logger"
	"time"
)

// Set 设置缓存
func (cls *Cache) Set(key string, value string, expire time.Duration) error {
	var v = Value{Bytes: []byte(value), Expired: time.Now().Add(expire).Unix()}
	var bs, err = json.Marshal(v)

	if nil != err {
		logger.GetLogger().Sugar().Errorw("Cache Set Marshal Fail.", "key", key, "value", value, "expire", expire, "err", err)

		return errors.New("设置缓存失败")
	}

	if err := cls.cache.Set(key, bs); nil != err {
		logger.GetLogger().Sugar().Errorw("Cache Set Fail.", "key", key, "value", value, "expire", expire, "err", err)

		return errors.New("设置缓存失败")
	}

	return nil
}
