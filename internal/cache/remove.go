package cache

// Remove 移除给定的key，并返回给定key的值
func (cls *Cache) Remove(key string) (string, error) {
	var v, err = cls.Get(key)
	if nil != err {
		return "", err
	}

	if err := cls.Del(key); nil != err {
		return "", err
	}

	return v, nil
}
