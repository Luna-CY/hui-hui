//go:build wireinject
// +build wireinject

package application

import (
	"gitee.com/Luna-CY/hui-hui/internal/application/caddy"
	"gitee.com/Luna-CY/hui-hui/internal/interface/application"
	"gitee.com/Luna-CY/hui-hui/internal/toolbox/crontab"
	"gitee.com/Luna-CY/hui-hui/internal/toolbox/processor"
	"github.com/google/wire"
)

func GetCaddyApplication() application.Caddy {
	panic(wire.Build(
		crontab.Single,
		processor.Single,
		caddy.Single,
		wire.Bind(new(application.Caddy), new(*caddy.Caddy)),
	))
}
