package app

import (
	"gitee.com/Luna-CY/hui-hui/internal/interface/application"
	"sync"
)

// Application 基础应用抽象
type Application[C any] struct {
	mutex  sync.Mutex // 配置锁，配置变更必须获得配置锁
	config *C

	state struct {
		mutex   sync.Mutex        // 读写锁，状态变更必须取得锁
		state   application.State // 当前状态
		message string            // 状态信息，一般为错误信息

		gp    float64 // 总进度
		stage string  // 阶段，字符串描述
		sp    float64 // 阶段装进度
	}
}
