package app

import "gitee.com/Luna-CY/hui-hui/internal/interface/application"

func (cls *Application[C]) State() (application.State, float64, string, string, float64) {
	return cls.GetState()
}
