package app

import (
	"gitee.com/Luna-CY/hui-hui/internal/interface/application"
	"gitee.com/Luna-CY/hui-hui/internal/toolbox/processor"
)

// ProcessStateMapping 状态映射表
var ProcessStateMapping = map[processor.State]application.State{
	processor.StateStarting:    application.Starting,
	processor.StateStarted:     application.Running,
	processor.StateStartFailed: application.RunError,
	processor.StateRunError:    application.RunError,
	processor.StateRestarting:  application.Restarting,
	processor.StateReloading:   application.Reloading,
	processor.StateStopping:    application.Stopping,
	processor.StateStopped:     application.Stopped,
}

// LockState 获取状态锁
func (cls *Application[C]) LockState() {
	cls.state.mutex.Lock()
}

// UnlockState 释放状态锁
func (cls *Application[C]) UnlockState() {
	cls.state.mutex.Unlock()
}

// GetState 获取当前状态
func (cls *Application[C]) GetState() (application.State, float64, string, string, float64) {
	return cls.state.state, cls.state.gp, cls.state.message, cls.state.stage, cls.state.sp
}

// UpdateState 更新状态数据
func (cls *Application[C]) UpdateState(state application.State, gp float64, message string) {
	cls.state.state = state
	cls.state.gp = gp
	cls.state.message = message
}

// UpdateStageProgress 更新阶段进度
func (cls *Application[C]) UpdateStageProgress(stage string, sp float64) {
	cls.state.stage = stage
	cls.state.sp = sp
}
