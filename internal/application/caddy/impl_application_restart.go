package caddy

import (
	"context"
)

func (cls *Caddy) Restart(ctx context.Context) error {
	if err := cls.Stop(ctx); nil != err {
		return err
	}

	return cls.Start(ctx, true)
}
