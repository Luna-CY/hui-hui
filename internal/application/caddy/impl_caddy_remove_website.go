package caddy

import "context"

func (cls *Caddy) RemoveWebsite(ctx context.Context, hostname string) error {
	cls.LockConfig()
	defer cls.UnlockConfig()

	var config = cls.GetConfig()
	if _, ok := config.Websites[hostname]; ok {
		delete(config.Websites, hostname)
		cls.SetConfig(config)
	}

	return cls.SyncToDisk(ctx, internalConfigPath)
}
