package caddy

import (
	"context"
	"gitee.com/Luna-CY/hui-hui/internal/application/app"
	"gitee.com/Luna-CY/hui-hui/internal/interface/application"
	"gitee.com/Luna-CY/hui-hui/internal/logger"
	"gitee.com/Luna-CY/hui-hui/internal/toolbox/processor"
	"gitee.com/Luna-CY/hui-hui/internal/util/point"
	"path/filepath"
)

func (cls *Caddy) Start(ctx context.Context, restart bool) error {
	if err := cls.ApplyConfig(ctx); nil != err {
		return err
	}

	var state, _, _, _, _ = cls.GetState()
	if application.Running == state || application.Reloading == state {
		return nil
	}

	logger.GetLogger().Sugar().Infof("启动Caddy服务: %s --config %s", filepath.Join(rootPath, "caddy"), configFilePath)

	if err := cls.processor.StartApplication("Caddy", filepath.Join(rootPath, "caddy"), []string{"run", "--config", configFilePath}, nil, rootPath, restart, func(state processor.State, err error) {
		cls.UpdateState(app.ProcessStateMapping[state], 0, point.ErrorMessage(err))
	}); nil != err {
		cls.UpdateState(application.RunError, 0, err.Error())
	}

	return nil
}
