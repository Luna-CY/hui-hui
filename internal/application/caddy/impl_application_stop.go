package caddy

import (
	"context"
	"gitee.com/Luna-CY/hui-hui/internal/interface/application"
	"gitee.com/Luna-CY/hui-hui/internal/logger"
)

func (cls *Caddy) Stop(_ context.Context) error {
	cls.LockState()
	defer cls.UnlockState()

	var state, _, _, _, _ = cls.GetState()
	if application.Running != state {
		return nil
	}

	if err := cls.processor.StopApplication("Caddy"); nil != err {
		return err
	}

	logger.GetLogger().Sugar().Info("Caddy服务已停止")

	return nil
}
