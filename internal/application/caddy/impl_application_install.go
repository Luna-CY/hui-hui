package caddy

import (
	"context"
	"gitee.com/Luna-CY/hui-hui/internal/interface/application"
	"gitee.com/Luna-CY/hui-hui/internal/logger"
	"gitee.com/Luna-CY/hui-hui/internal/runtime"
	"gitee.com/Luna-CY/hui-hui/internal/util/archive"
	"gitee.com/Luna-CY/hui-hui/internal/util/downloader"
	"gitee.com/Luna-CY/hui-hui/internal/util/syscall"
	"math"
	"os"
	runtime2 "runtime"
	"strings"
)

const packageAddress = "https://github.com/caddyserver/caddy/releases/download/v{version}/caddy_{version}_{os}_{arch}.tar.gz"

func (cls *Caddy) Install(ctx context.Context, overwrite bool) error {
	if state, _, _, _, _ := cls.GetState(); application.Installed == state && !overwrite {
		logger.GetLogger().Sugar().Info("本地Caddy服务符合版本要求，取消安装")

		return nil
	}

	cls.LockState()
	defer cls.UnlockState()

	cls.UpdateState(application.Installing, 0, "")
	cls.UpdateStageProgress("预清理本地Caddy服务安装路径", 0)

	// 完全删除Caddy服务器目录
	logger.GetLogger().Sugar().Info("预清理本地Caddy服务安装路径")

	if _, err := syscall.ExecuteCommandGetOutputIgnoreQuitCode(ctx, "rm", "-rf", rootPath); nil != err {
		cls.UpdateState(application.InstallFailure, 0, err.Error())

		return err
	}

	// 创建路径
	if err := os.MkdirAll(rootPath, 0755); nil != err {
		cls.UpdateState(application.InstallFailure, 0, err.Error())

		return err
	}

	cls.UpdateState(application.Installing, 20, "")
	cls.UpdateStageProgress("下载安装包", 0)

	// 下载文件
	var keywords = []string{"{version}", runtime.CaddyMinimumVersion, "{os}", runtime2.GOOS, "{arch}", runtime2.GOARCH}
	var address = strings.NewReplacer(keywords...).Replace(packageAddress)
	logger.GetLogger().Sugar().Infof("下载文件: %s", address)

	size, file, err := downloader.DownloadToTempFile(ctx, address, func(total int64, current int64) {
		cls.UpdateStageProgress("下载安装包", math.Ceil(float64(current)/float64(total)*10000)/100)
	})
	if nil != err {
		cls.UpdateState(application.InstallFailure, 20, err.Error())

		return err
	}

	defer func() {
		_ = os.RemoveAll(file.Name())
	}()

	cls.UpdateState(application.Installing, 50, "")
	cls.UpdateStageProgress("安装Caddy服务", 0)

	// 解压
	logger.GetLogger().Sugar().Infof("解压文件: %s", file.Name())
	if err := archive.DecompressTarGzipFile(ctx, file, size, rootPath, nil); nil != err {
		cls.UpdateState(application.InstallFailure, 50, err.Error())

		return err
	}

	// 初始化基础配置
	cls.UpdateState(application.Installing, 70, "")
	cls.UpdateStageProgress("清理旧的配置数据", 0)

	// 清理目录
	if err := os.RemoveAll(configFolder); nil != err {
		cls.UpdateState(application.InstallFailure, 70, err.Error())

		return err
	}

	// 重建目录
	if err := os.MkdirAll(configLoadPath, 0755); nil != err {
		cls.UpdateState(application.InstallFailure, 70, err.Error())

		return err
	}

	cls.UpdateState(application.Installed, 100, "")
	cls.UpdateStageProgress("-", 0)

	return nil
}
