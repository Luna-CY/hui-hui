package caddy

import (
	"context"
	"gitee.com/Luna-CY/hui-hui/internal/interface/application"
	"gitee.com/Luna-CY/hui-hui/internal/util/syscall"
	"path/filepath"
)

func (cls *Caddy) Reload(ctx context.Context) error {
	cls.LockState()
	defer cls.UnlockState()

	var state, _, _, _, _ = cls.GetState()
	if application.Running != state {
		return nil
	}

	// Caddy服务的reload需要使用自定义的命令
	cls.UpdateState(application.Reloading, 0, "")
	if _, err := syscall.ExecuteCommandGetOutputIgnoreQuitCode(ctx, filepath.Join(rootPath, "caddy"), "reload", "--config", configFilePath); nil != err {
		cls.UpdateState(application.Running, 0, err.Error())

		return err
	}

	cls.UpdateState(application.Running, 0, "")

	return nil
}
