package caddy

import (
	"context"
	"gitee.com/Luna-CY/hui-hui/internal/interface/application"
)

func (cls *Caddy) SetBasic(ctx context.Context, basic application.CaddyBasic) error {
	cls.LockConfig()
	defer cls.UnlockConfig()

	var config = cls.GetConfig()
	config.Basic = basic
	cls.SetConfig(config)

	return cls.SyncToDisk(ctx, internalConfigPath)
}
