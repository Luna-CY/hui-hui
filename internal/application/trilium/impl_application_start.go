package trilium

import (
	"context"
	"fmt"
	"gitee.com/Luna-CY/hui-hui/internal/application/app"
	"gitee.com/Luna-CY/hui-hui/internal/interface/application"
	"gitee.com/Luna-CY/hui-hui/internal/logger"
	"gitee.com/Luna-CY/hui-hui/internal/toolbox/processor"
	"gitee.com/Luna-CY/hui-hui/internal/util/point"
	"path/filepath"
)

func (cls *Trilium) Start(ctx context.Context, restart bool) error {
	if err := cls.ApplyConfig(ctx); nil != err {
		return err
	}

	var state, _, _, _, _ = cls.GetState()
	if application.Running == state || application.Reloading == state {
		return nil
	}

	logger.GetLogger().Sugar().Infof("启动Trilium服务: %s %s", filepath.Join(rootPath, "node", "bin", "node"), filepath.Join("src", "www"))

	var environments = []string{fmt.Sprintf("TRILIUM_DATA_DIR=%s", dataDir)}
	if err := cls.processor.StartApplication("Trilium", filepath.Join(rootPath, "node", "bin", "node"), []string{filepath.Join("src", "www")}, environments, rootPath, restart, func(state processor.State, err error) {
		cls.UpdateState(app.ProcessStateMapping[state], 0, point.ErrorMessage(err))
	}); nil != err {
		cls.UpdateState(application.RunError, 0, err.Error())
	}

	return nil
}
