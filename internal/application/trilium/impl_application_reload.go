package trilium

import (
	"context"
	"gitee.com/Luna-CY/hui-hui/internal/interface/application"
)

func (cls *Trilium) Reload(_ context.Context) error {
	cls.LockState()
	defer cls.UnlockState()

	// 检查状态，非运行状态不处理
	var state, _, _, _, _ = cls.GetState()
	if application.Running != state {
		return nil
	}

	if err := cls.processor.ReloadApplication("Trilium"); nil != err {
		return err
	}

	return nil
}
