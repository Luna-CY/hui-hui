package trilium

import "gitee.com/Luna-CY/hui-hui/internal/interface/application"

func (cls *Trilium) SetConfig(config *application.TriliumConfig) error {
	cls.Application.SetConfig(config)

	return nil
}
