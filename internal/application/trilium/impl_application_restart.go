package trilium

import "context"

func (cls *Trilium) Restart(ctx context.Context) error {
	if err := cls.Stop(ctx); nil != err {
		return err
	}

	return cls.Start(ctx, true)
}
