package v2ray

import (
	"context"
	"errors"
	"gitee.com/Luna-CY/hui-hui/internal/interface/application"
)

func (cls *V2ray) SetInbound(ctx context.Context, id string, bound *application.V2rayBound) error {
	if application.V2rayBoundProtocolDokodemoDoor != bound.Protocol && application.V2rayBoundProtocolHttp != bound.Protocol &&
		application.V2rayBoundProtocolMTProto != bound.Protocol && application.V2rayBoundProtocolShadowSocks != bound.Protocol &&
		application.V2rayBoundProtocolSocks != bound.Protocol && application.V2rayBoundProtocolVMess != bound.Protocol {
		return errors.New("无效的入站协议")
	}

	cls.LockConfig()
	defer cls.UnlockConfig()

	var config = cls.GetConfig()
	config.Inbounds[id] = bound
	cls.SetConfig(config)

	return cls.SyncToDisk(ctx, internalConfigPath)
}
