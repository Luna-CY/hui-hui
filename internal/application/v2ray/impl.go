package v2ray

import (
	"context"
	"gitee.com/Luna-CY/hui-hui/internal/application/app"
	"gitee.com/Luna-CY/hui-hui/internal/interface/application"
	"gitee.com/Luna-CY/hui-hui/internal/logger"
	"gitee.com/Luna-CY/hui-hui/internal/runtime"
	"gitee.com/Luna-CY/hui-hui/internal/toolbox/processor"
	"os"
	"path/filepath"
	"sync"
)

// rootPath V2ray服务器目录
var rootPath = filepath.Join(runtime.ApplicationRoot, "v2ray")

// internalConfigPath 内部配置文件路径
var internalConfigPath = filepath.Join(runtime.ConfigRoot, "v2ray.v1.json")

// logFolder 日志目录
var logFolder = filepath.Join(rootPath, "logs")

// configFolder 配置目录
var configFolder = filepath.Join(rootPath, "huihui")

// configFilePath V2ray服务器配置文件路径
var configFilePath = filepath.Join(configFolder, "config.json")

var once sync.Once
var instance *V2ray

// Single 返回V2ray应用单例对象
func Single(processor *processor.Processor) *V2ray {
	once.Do(func() {
		var config = new(application.V2rayConfig)
		config.Inbounds = map[string]*application.V2rayBound{}
		config.Outbounds = map[string]*application.V2rayBound{}

		instance = &V2ray{processor: processor}
		instance.UpdateState(application.NotInstall, 0, "")
		instance.SetConfig(config)

		// 本地自检
		if err := instance.CheckLocationState(context.Background()); nil != err {
			logger.GetLogger().Sugar().Errorf("检查V2ray服务状态失败，失败原因: %s", err)

			os.Exit(1)
		}

		// 加载本地配置
		if err := instance.LoadFromDisk(nil, internalConfigPath); nil != err {
			logger.GetLogger().Sugar().Errorf("加载V2ray配置文件失败，失败原因: %s", err)

			os.Exit(1)
		}
	})

	return instance
}

type V2ray struct {
	app.Application[application.V2rayConfig]

	processor *processor.Processor
}
