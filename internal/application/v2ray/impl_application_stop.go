package v2ray

import (
	"context"
	"gitee.com/Luna-CY/hui-hui/internal/interface/application"
	"gitee.com/Luna-CY/hui-hui/internal/logger"
)

func (cls *V2ray) Stop(_ context.Context) error {
	cls.LockState()
	defer cls.UnlockState()

	var state, _, _, _, _ = cls.GetState()
	if application.Running != state {
		return nil
	}

	if err := cls.processor.StopApplication("V2ray"); nil != err {
		return err
	}

	logger.GetLogger().Sugar().Info("V2ray服务已停止")

	return nil
}
