package v2ray

import "context"

func (cls *V2ray) RemoveInbound(ctx context.Context, id string) error {
	cls.LockConfig()
	defer cls.UnlockConfig()

	var config = cls.GetConfig()
	if _, ok := config.Inbounds[id]; ok {
		delete(config.Inbounds, id)
		cls.SetConfig(config)
	}

	return cls.SyncToDisk(ctx, internalConfigPath)
}
