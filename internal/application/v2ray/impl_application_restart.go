package v2ray

import "context"

func (cls *V2ray) Restart(ctx context.Context) error {
	if err := cls.Stop(ctx); nil != err {
		return err
	}

	return cls.Start(ctx, true)
}
