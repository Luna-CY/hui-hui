package v2ray

import "context"

func (cls *V2ray) RemoveOutbound(ctx context.Context, id string) error {
	cls.LockConfig()
	defer cls.UnlockConfig()

	var config = cls.GetConfig()
	if _, ok := config.Outbounds[id]; ok {
		delete(config.Outbounds, id)
		cls.SetConfig(config)
	}

	return cls.SyncToDisk(ctx, internalConfigPath)
}
