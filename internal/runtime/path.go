package runtime

import "path/filepath"

// Root 根目录
var Root = filepath.Join("/opt", "huihui")

// MainConfig 主配置文件
var MainConfig = filepath.Join(Root, "hui-hui.toml")

// ConfigRoot 配置根目录
var ConfigRoot = filepath.Join(Root, "configs")

// ApplicationRoot 应用根目录
var ApplicationRoot = filepath.Join(Root, "applications")

// Data 数据目录
var Data = filepath.Join("/data", "huihui")
