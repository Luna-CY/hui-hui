package runtime

import (
	"context"
	"os"
	"os/signal"
	"sync"
	"syscall"
)

// RegisterSystemSignal 注册信号
func RegisterSystemSignal(cancel context.CancelFunc, reload func()) {
	var signals = make(chan os.Signal, 1)

	var wg sync.WaitGroup
	wg.Add(1)

	go func() {
		wg.Done()

		signal.Notify(signals, syscall.SIGTERM, syscall.SIGINT, syscall.SIGUSR2)
		for {
			switch <-signals {
			case syscall.SIGUSR2:
				reload()
			default:
				cancel()

				return
			}
		}
	}()

	wg.Wait()
}
