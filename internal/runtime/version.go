package runtime

// VersionName 语义化版本号
const VersionName = "0.4.0"

// CaddyMinimumVersion Caddy服务的最小版本号
const CaddyMinimumVersion = "2.6.4"

// V2rayMinimumVersion V2ray服务的最小版本号
const V2rayMinimumVersion = "5.7.0"

// TriliumMinimumVersion Trilium服务的最小版本号
const TriliumMinimumVersion = "0.60.4"
