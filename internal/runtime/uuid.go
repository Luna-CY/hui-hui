package runtime

import "github.com/google/uuid"

var Uuid = uuid.New().String()
