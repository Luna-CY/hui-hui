package configure

type AuthenticationType string

const (
	AuthenticationTypeFixed     = AuthenticationType("fixed")      // 固定口令
	AuthenticationTypeEmailCode = AuthenticationType("email-code") // 邮件验证码
	AuthenticationTypeTOTP      = AuthenticationType("totp")       // 随机验证码
)

type c struct {
	Server struct {
		Hostname string `mapstructure:"hostname" toml:"hostname"`           // HuiHui的域名
		Port     int    `mapstructure:"port" toml:"port,omitempty"`         // 监听端口
		GinMode  string `mapstructure:"gin-mode" toml:"gin-mode,omitempty"` // Gin框架模式

		Authentication struct {
			Type     AuthenticationType `mapstructure:"type" toml:"type,omitempty"`         // 配置类型
			Password string             `mapstructure:"password" toml:"password,omitempty"` // 类型为 AuthenticationTypeFixed 时有效，为登录口令
			Email    string             `mapstructure:"email" toml:"email,omitempty"`       // 接受验证码的邮件地址，类型为 AuthenticationTypeEmailCode 时有效，启用此项时SMTP配置必须有效
			Secret   string             `mapstructure:"secret" toml:"secret,omitempty"`     // 随机验证码生成使用的字符串值，类型为 AuthenticationTypeTOTP 时有效，此项必须与手机端生成器使用的一致
		} `mapstructure:"authentication" toml:"authentication,omitempty"` // 鉴权配置
	} `mapstructure:"server" toml:"server,omitempty"` // 服务器配置

	Smtp struct {
		Host     string `mapstructure:"host" toml:"host,omitempty"`         // 服务器地址
		Port     int    `mapstructure:"port" toml:"port,omitempty"`         // 服务器端口
		Username string `mapstructure:"username" toml:"username,omitempty"` // 用户名
		Password string `mapstructure:"password" toml:"password,omitempty"` // 密码
		From     string `mapstructure:"from" toml:"from,omitempty"`         // 发信人
	} `mapstructure:"smtp" toml:"smtp,omitempty"` // SMTP服务器配置
}

var Configure = new(c)
