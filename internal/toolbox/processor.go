package toolbox

import (
	"context"
	"gitee.com/Luna-CY/hui-hui/internal/interface/runner"
	"gitee.com/Luna-CY/hui-hui/internal/logger"
	"gitee.com/Luna-CY/hui-hui/internal/util/goroutine"
)

func NewProcessor() runner.Runner {
	return func(ctx context.Context) (<-chan struct{}, error) {
		var ch = make(chan struct{}, 1)

		var processor = GetProcessor()
		goroutine.Go(func() {
			for {
				select {
				case <-ctx.Done():
					if err := processor.Quit(); nil != err {
						logger.GetLogger().Sugar().Errorf("进程管理器退出失败: %s", err)
					} else {
						logger.GetLogger().Sugar().Info("进程管理器已退出")
					}

					ch <- struct{}{}

					return
				}
			}
		})

		return ch, nil
	}
}
