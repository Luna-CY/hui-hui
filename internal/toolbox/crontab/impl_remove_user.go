package crontab

// RemoveUser 移除用户任务
func (cls *Crontab) RemoveUser(id string) error {
	cls.mutex.Lock()
	defer cls.mutex.Unlock()

	var length = len(cls.user)
	for index, task := range cls.user {
		if task.Id == id {
			cls.user = append(cls.user[:index], cls.user[index+1:]...)
		}
	}

	// 如果未删除任何任务直接返回
	if len(cls.user) == length {
		return nil
	}

	return cls.sync()
}
