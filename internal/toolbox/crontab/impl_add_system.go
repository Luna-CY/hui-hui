package crontab

import "github.com/google/uuid"

// AddSystem 添加系统任务
func (cls *Crontab) AddSystem(schedule string, callback func()) error {
	cls.mutex.Lock()
	defer cls.mutex.Unlock()

	id, err := cls.cron.AddFunc(schedule, callback)
	if nil != err {
		return err
	}

	var task = &Task{Id: uuid.New().String(), Schedule: schedule, EntryId: id, Callback: callback}
	cls.system = append(cls.system, task)

	return nil
}
