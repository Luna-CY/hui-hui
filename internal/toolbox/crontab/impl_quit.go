package crontab

import (
	"context"
	"errors"
	"time"
)

// Quit 退出定时器
func (cls *Crontab) Quit() error {
	var ctx, cancel = context.WithTimeout(context.Background(), 5*time.Second)
	defer cancel()

	select {
	case <-ctx.Done():
		return errors.New("定时器退出超时")
	case <-cls.cron.Stop().Done():
		return nil
	}
}
