package downloader

import (
	"context"
	"time"
)

// WaitCancel 等待所有任务取消，5秒超时
func (cls *Downloader) WaitCancel() {
	var ctx, cancel = context.WithTimeout(context.Background(), 5*time.Second)
	defer cancel()

	for {
		if nil != ctx.Err() {
			return
		}

		var downloading bool

		cls.mutex.RLock()
		for _, task := range cls.tasks {
			if TaskStateDownloading == task.State {
				downloading = true
				break
			}
		}
		cls.mutex.RUnlock()

		if !downloading {
			return
		}
	}
}
