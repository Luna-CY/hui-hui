package downloader

import (
	"gitee.com/Luna-CY/hui-hui/internal/util/structs"
	"sort"
)

// Tasks 返回任务列表，并保持按开始时间降序
func (cls *Downloader) Tasks() []*Task {
	cls.mutex.RLock()
	defer cls.mutex.RUnlock()

	var tasks = structs.MapValues(cls.tasks)
	sort.Slice(tasks, func(i, j int) bool {
		return tasks[i].StartTime.Unix() > tasks[j].StartTime.Unix()
	})

	return tasks
}
