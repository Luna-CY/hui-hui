package downloader

// Cancel 取消下载任务
func (cls *Downloader) Cancel(id string) error {
	cls.mutex.RLock()
	defer cls.mutex.RUnlock()

	if task, ok := cls.tasks[id]; ok {
		if TaskStateDownloading != task.State {
			return nil
		}

		task.cancel()
	}

	return nil
}
