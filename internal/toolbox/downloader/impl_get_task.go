package downloader

// GetTask 获取任务
func (cls *Downloader) GetTask(id string) (*Task, bool) {
	cls.mutex.RLock()
	defer cls.mutex.RUnlock()

	if task, ok := cls.tasks[id]; ok {
		return task, true
	}

	return nil, false
}
