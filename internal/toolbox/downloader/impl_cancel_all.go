package downloader

// CancelAll 取消所有进行中的任务，该方法会标记下载器为退出
func (cls *Downloader) CancelAll() {
	cls.mutex.Lock()
	defer cls.mutex.Unlock()

	cls.quit = true
	for _, task := range cls.tasks {
		if TaskStateDownloading != task.State {
			continue
		}

		task.cancel()
	}
}
