//go:build wireinject
// +build wireinject

package toolbox

import (
	"gitee.com/Luna-CY/hui-hui/internal/toolbox/crontab"
	"gitee.com/Luna-CY/hui-hui/internal/toolbox/downloader"
	"gitee.com/Luna-CY/hui-hui/internal/toolbox/processor"
	"gitee.com/Luna-CY/hui-hui/internal/toolbox/resources"
	"github.com/google/wire"
)

func GetDownloader() *downloader.Downloader {
	panic(wire.Build(
		resources.Single,
		downloader.Single,
	))
}

func GetCrontab() *crontab.Crontab {
	panic(wire.Build(
		crontab.Single,
	))
}

func GetProcessor() *processor.Processor {
	panic(wire.Build(
		crontab.Single,
		processor.Single,
	))
}
