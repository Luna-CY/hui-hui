package toolbox

import (
	"context"
	"gitee.com/Luna-CY/hui-hui/internal/interface/runner"
	"gitee.com/Luna-CY/hui-hui/internal/logger"
	"gitee.com/Luna-CY/hui-hui/internal/util/goroutine"
)

func NewCrontab() runner.Runner {
	return func(ctx context.Context) (<-chan struct{}, error) {
		var ch = make(chan struct{}, 1)

		var crontab = GetCrontab()
		goroutine.Go(func() {
			for {
				select {
				case <-ctx.Done():
					if err := crontab.Quit(); nil != err {
						logger.GetLogger().Sugar().Errorf("定时器退出失败: %s", err)
					} else {
						logger.GetLogger().Sugar().Info("定时器已退出")
					}

					ch <- struct{}{}

					return
				}
			}
		})

		return ch, nil
	}
}
