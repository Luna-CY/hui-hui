package processor

import (
	"syscall"
)

// Kill 发送信号
func (cls *Processor) Kill(id int32, signal syscall.Signal) error {
	cls.mutex.RLock()
	defer cls.mutex.RUnlock()

	if process, ok := cls.system[id]; ok {
		if err := process.SendSignal(signal); nil != err {
			return err
		}
	}

	return nil
}
