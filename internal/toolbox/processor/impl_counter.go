package processor

import (
	"gitee.com/Luna-CY/hui-hui/internal/logger"
	"github.com/shirou/gopsutil/v3/process"
)

// counter 进程信息统计
func (cls *Processor) counter() {
	cls.mutex.Lock()
	defer cls.mutex.Unlock()

	if cls.quiting {
		return
	}

	var pids, err = process.Pids()
	if nil != err {
		logger.GetLogger().Sugar().Errorf("进程管理器: 查询进程ID数据失败，失败原因: %s", err)

		return
	}

	cls.system = make(map[int32]*process.Process)
	for _, pid := range pids {
		var p, err = process.NewProcess(pid)
		if nil != err {
			logger.GetLogger().Sugar().Errorf("进程管理器: 获取进程信息失败，失败原因: %s，进程ID: %d", err, pid)

			continue
		}

		running, err := p.IsRunning()
		if nil != err {
			logger.GetLogger().Sugar().Errorf("进程管理器: 获取进程状态信息失败，失败原因: %s，进程ID: %d", err, pid)

			continue
		}

		if !running {
			continue
		}

		cls.system[pid] = p
	}
}
