package processor

import (
	"syscall"
)

// ReloadApplication 重载应用配置
func (cls *Processor) ReloadApplication(name string) error {
	cls.mutex.RLock()
	defer cls.mutex.RUnlock()

	if application, ok := cls.application[name]; ok {
		application.quiting = true
		application.state = StateReloading
		application.callback(StateReloading, nil)
		if err := application.c.Process.Signal(syscall.SIGUSR2); nil != err {
			application.state = StateStarted
			application.callback(StateStarted, nil)

			return err
		}

		application.state = StateStarted
		application.callback(StateStarted, nil)
	}

	return nil
}
