package processor

import (
	"gitee.com/Luna-CY/hui-hui/internal/logger"
	"sort"
)

// GetProcessList 获取系统进程列表
func (cls *Processor) GetProcessList() []Process {
	cls.mutex.RLock()
	defer cls.mutex.RUnlock()

	var processes []Process
	for _, p := range cls.system {
		ppid, err := p.Ppid()
		if nil != err {
			logger.GetLogger().Sugar().Errorf("进程管理器: 获取父进程ID失败，失败原因: %s，进程ID: %d", err, p.Pid)
		}

		username, err := p.Username()
		if nil != err {
			logger.GetLogger().Sugar().Errorf("进程管理器: 获取用户名称失败，失败原因: %s，进程ID: %d", err, p.Pid)
		}

		command, err := p.Cmdline()
		if nil != err {
			logger.GetLogger().Sugar().Errorf("进程管理器: 获取命令信息失败，失败原因: %s，进程ID: %d", err, p.Pid)
		}

		processes = append(processes, Process{Pid: p.Pid, PPid: ppid, Username: username, Command: command})
	}

	sort.Slice(processes, func(i, j int) bool {
		return processes[i].Pid < processes[j].Pid
	})

	return processes
}
