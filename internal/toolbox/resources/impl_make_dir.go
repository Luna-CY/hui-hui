package resources

import (
	"os"
	"path/filepath"
)

// MakeDir 创建目录
func (cls *Resources) MakeDir(paths []string) error {
	path, err := cls.normal(paths)
	if nil != err {
		return err
	}

	if err := os.MkdirAll(filepath.Join(cls.path, path), 0755); nil != err {
		return err
	}

	return nil
}
