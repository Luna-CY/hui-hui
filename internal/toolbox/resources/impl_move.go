package resources

import (
	"context"
	"errors"
	"gitee.com/Luna-CY/hui-hui/internal/util/syscall"
	"path/filepath"
)

func (cls *Resources) Move(source []string, target []string) error {
	var sp, err = cls.normal(source)
	if nil != err {
		return err
	}

	tp, err := cls.normal(target)
	if nil != err {
		return err
	}

	output, err := syscall.ExecuteCommandGetOutputIgnoreQuitCode(context.Background(), "mv", filepath.Join(cls.path, sp), filepath.Join(cls.path, tp))
	if nil != err {
		return err
	}

	if 0 != len(output) {
		return errors.New("移动失败，请检查源路径与目标路径")
	}

	return nil
}
