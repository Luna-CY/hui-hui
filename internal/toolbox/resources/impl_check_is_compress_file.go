package resources

import (
	"errors"
	"os"
	"path/filepath"
)

// CheckIsCompressedFile 检查是否是压缩文件
func (cls *Resources) CheckIsCompressedFile(paths []string) (bool, error) {
	var path, err = cls.normal(paths)
	if nil != err {
		return false, err
	}

	path = filepath.Join(cls.path, path)
	stat, err := os.Stat(path)
	if nil != err && !os.IsNotExist(err) {
		return false, err
	}

	if os.IsNotExist(err) {
		return false, errors.New("路径不存在")
	}

	if stat.IsDir() {
		return false, errors.New("不是文件")
	}

	var ext = filepath.Ext(path)
	if ".gz" != ext && ".xz" != ext && ".zip" != ext {
		return false, nil
	}

	return true, nil
}
