package resources

type EntryState int

const (
	EntryStateNormal        = EntryState(iota) + 1 // 正常
	EntryStateCompressing                          // 压缩中
	EntryStateDecompressing                        // 解压中
)

type Entry struct {
	Name             string     // 文件或文件夹名称
	IsDir            bool       // 是否是目录
	Size             int64      // 文件大小，单位byte，文件夹无效
	Ext              string     // 后缀
	IsCompressedFile bool       // 是否是压缩文件
	State            EntryState // 状态
	UpdateTime       int64      // 更新时间，10位时间戳
}
