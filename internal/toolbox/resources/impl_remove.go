package resources

import (
	"os"
	"path/filepath"
)

// Remove 移除目录或文件
func (cls *Resources) Remove(paths []string) error {
	var path, err = cls.normal(paths)
	if nil != err {
		return err
	}

	if err := os.RemoveAll(filepath.Join(cls.path, path)); nil != err {
		return err
	}

	return nil
}
