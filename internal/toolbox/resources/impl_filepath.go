package resources

import "path/filepath"

// AbsFilepath 返回路径与文件在系统的绝对路径
func (cls *Resources) AbsFilepath(paths []string, name string) (string, error) {
	var path, err = cls.normal(paths)
	if nil != err {
		return "", err
	}

	return filepath.Join(cls.path, path, name), nil
}
