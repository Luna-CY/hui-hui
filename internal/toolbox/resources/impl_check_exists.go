package resources

import (
	"os"
	"path/filepath"
)

// CheckExists 检查路径是否存在
func (cls *Resources) CheckExists(paths []string) (bool, error) {
	var path, err = cls.normal(paths)
	if nil != err {
		return false, err
	}

	path = filepath.Join(cls.path, path)
	_, err = os.Stat(path)
	if nil != err && !os.IsNotExist(err) {
		return false, err
	}

	if os.IsNotExist(err) {
		return false, nil
	}

	return true, nil
}
