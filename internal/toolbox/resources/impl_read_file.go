package resources

import (
	"errors"
	"gitee.com/Luna-CY/hui-hui/internal/logger"
	"io"
	"os"
	"path/filepath"
)

// ReadFile 读取文件内容
func (cls *Resources) ReadFile(paths []string) ([]byte, error) {
	var path, err = cls.normal(paths)
	if nil != err {
		return nil, err
	}

	stat, err := os.Stat(filepath.Join(cls.path, path))
	if nil != err && !os.IsNotExist(err) {
		return nil, err
	}

	if os.IsNotExist(err) {
		return nil, errors.New("文件不存在")
	}

	if stat.IsDir() {
		return nil, errors.New("无法读取目录内容")
	}

	f, err := os.Open(filepath.Join(cls.path, path))
	if nil != err {
		return nil, err
	}

	defer func() {
		if err := f.Close(); nil != err {
			logger.GetLogger().Sugar().Errorf("资源管理器: 关闭文件失败，%s", err)
		}
	}()

	return io.ReadAll(f)
}
