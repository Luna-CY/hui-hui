package resources

import (
	"errors"
	"gitee.com/Luna-CY/hui-hui/internal/logger"
	"io"
	"os"
	"path/filepath"
)

// SaveFile 保存文件，文件不存在时创建，否则完全覆盖
func (cls *Resources) SaveFile(paths []string, content io.Reader) error {
	var path, err = cls.normal(paths)
	if nil != err {
		return err
	}

	stat, err := os.Stat(filepath.Join(cls.path, path))
	if nil != err && !os.IsNotExist(err) {
		return err
	}

	if nil != stat && stat.IsDir() {
		return errors.New("目标位置已存在且为一个目录")
	}

	f, err := os.OpenFile(filepath.Join(cls.path, path), os.O_CREATE|os.O_TRUNC|os.O_WRONLY, 0644)
	if nil != err {
		return err
	}

	defer func() {
		if err := f.Close(); nil != err {
			logger.GetLogger().Sugar().Errorf("资源管理器: 关闭文件失败，%s", err)
		}
	}()

	if _, err := io.Copy(f, content); nil != err {
		return err
	}

	return nil
}
