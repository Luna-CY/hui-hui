.PHONY: wire
wire:
	dem wire gitee.com/Luna-CY/hui-hui/command/initializer
	dem wire gitee.com/Luna-CY/hui-hui/server/http/handler
	dem wire gitee.com/Luna-CY/hui-hui/internal/application
	dem wire gitee.com/Luna-CY/hui-hui/internal/toolbox

.PHONY: amd
amd: build-amd package-amd

.PHONY: arm
arm: build-arm package-arm

.PHONY: build-static
build-static:
	cd server/http/hui-hui && yarn run build

.PHONY: build
build:
	go build -o hui-hui ./hui-hui.go

.PHONY: build-amd
build-amd:
	GOOS=linux GOARCH=amd64 go build -o hui-hui ./hui-hui.go

.PHONY: build-arm
build-arm:
	GOOS=linux GOARCH=arm64 go build -o hui-hui ./hui-hui.go

.PHONY: package
package:
	tar zcf hui-hui.tar.gz hui-hui server/http/hui-hui/dist

.PHONY: package-amd
package-amd:
	tar zcf hui-hui_linux_x86_64_v$(VERSION).tar.gz hui-hui server/http/hui-hui/dist

.PHONY: package-arm
package-arm:
	tar zcf hui-hui_linux_aarch64_v$(VERSION).tar.gz hui-hui server/http/hui-hui/dist
